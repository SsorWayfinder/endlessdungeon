{
"input": "\n====================\n:",
"intro":
    '''
    You are winded as you land on the hard stones of the dungeon, and there is a loud clang as the grate above you is closed.
    You regain your breath and slowly start to make out the room around you.

    Welcome to the Endless Dungeon!

    Type "help" for information on what you can do.
    ''',
"support":
    '''
    If you want to see this game developed further,
    or want to have a say in what should be added please consider becoming a patron at:
    https://www.patreon.com/minimumviablegame

    If you want to aid in the development of the game, feel free to clone the repo at:
    https://gitlab.com/SsorWayfinder/endlessdungeon
    ''',
"quit":
    '''
    {0}
    Thank you for playing! Goodbye!
    ''',
"credits":
    '''
    Credits:

    Development:
    Ross Mattingly (https://gitlab.com/SsorWayfinder)

    Support and Direction (patrons):
    {0}

    If you want you name on these credits join my patreon at https://www.patreon.com/minimumviablegame
    ''',
"unknown": 'Invalid command. Use "HELP" to get a list of valid commands and how to use them.',
"command":
{
    "UNKNOWN": "",
    "HELP": "HELP - Print out this information.",
    "SUPPORT": "SUPPORT - Print out information on supporting this game.",
    "CREDITS": "CREDITS - Print out the credits of the game.",
    "QUIT": "QUIT - Stop playing.",
    "MOVE": "MOVE - Move your character into a new room. Is followed by a direction(LEFT, FORWARD, RIGHT, BACK, UP).",
    "DRINK": "DRINK - Consume any bottle of water in the room at the moment.",
    "GRAB": "GRAB - Place item from the room into your inventory. Followed by the number (position) of the item you want to grab.",
    "INVENTORY": "INVENTORY - List what is currently in your inventory.",
    "DROP":  "DROP - Drop an item in your inventory. Followed by the number (position) of the item you want to drop.",
    "LIGHT": "LIGHT - Light a candle in your inventory. Followed by the number (position) of the candle.",
    "EXTINGUISH": "EXTINGUISH - Put out all currently burning candles in your possession.",
    "LOOK": "LOOK - Look into a neighboring room. Followed by a direction(LEFT, FORWARD, RIGHT, BACK).",
    "SAY": "SAY - Speak out loud the words that follow the command.",
    "REFILL": "REFILL - Fills any empty vials in your inventory if you are in a room with a fountain."
},
"room":
{
    "dark": "You are unable to make out any of the features of the room through the thick darkness.",
    "lit": "The walls and floor are all made of the same bland gray stone.",
    "difficult":
    {
        0: "The floor is covered in rubble. Moving any direction but back will require you to exert yourself climbing over jagged stone.",
        1: "Fragments of a what appears to have been the statue of an enormous figure litter the floor. Moving any direction but back will require you to exert yourself climbing over marble limbs.",
        2: "The roots of a mighty tree fill the room. Climbing under and through the thick vegetation will be exhausting but necessary if you wish to go any direction but back.",
        3: "The ceiling of this room has fallen in leaving piles of stone, pipes, and beams in your way if you don't want to go back.",
        4: "You stand just on the edge of a great pit. You will have to scale its sides if you want to pass through this room.",
    },
    "items": "Sitting in the middle of the room there is:",
    "origin": "Dim light filters through a grate set in the ceiling."
},
"Vial":
{
    0: "A vial full of water.",
    1: "An empty vial."
},
"Candle":
{
    0: "A lit candle.",
    1: "An unlit, unused candle.",
    2: "An unlit, partially burnt candle.",
    3: "A burnt nub."
},
"Teleport":
{
    "engraving": "You see engraved on the ground: \"{0}\"",
    "teleport": "The world twists around you."
},
"End":
{
    "description": "A spiralling staircase extends up into a faint light.",
    "escape": "You climb the stairs, up, in to fresh air, sunlight, and freedom.\nYou Win!\n"
},
"Fountain":
{
    "description": "A small marble fountain quietly bubbles in the middle of the room.",
    "refill": "You carefully refill each of you your empty vials."
},
"Character":
{
    "drop":
    {
        "empty": "Nothing to drop!\n",
        "undefined": "Drop which?\n",
        "no_item": "No item in position \"{0}\"\n",
        "dropped": "Dropped:\n{0}"
        },
    "grab":
    {
        "empty": "Nothing to grab!\n",
        "undefined": "Grab which?\n",
        "no_item": "No item in position \"{0}\"\n",
        "grabbed": "Grabbed:\n{0}"
    },
    "light":
    {
        "success": "Candle lit.\n",
        "undefined": "Light which candle?\n",
        "no_candle": "You have no candles to light.\n",
        "bad_input": "\"{0}\" is not a number.\n",
        "no_item": "\"{0}\" is not a position in your inventory.\n",
        "not_candle": '"{0}" is {1} It cannot be lit.\n',
        "success_position": 'Candle "{0}" lit.\n'
    },
    "extinguish":
    {
        "success": "You blow all of your lit candles out.\n",
        "failure": "You you have nothing to extinguish.\n"
    },
    "inventory" : "Inventory:\n{0}",
    "drink":
    {
        "nothing_to_drink": "Nothing here to drink!\n",
        "not_thirsty": "You are not thirsty!\n",
        "success": "You feel refreshed!\n"
    },
    "move":
    {
        "bad_input": "I'm sorry, I don't know that direction. Please use:\n{0}",
        "no_exit": "There is no exit that direction.\n",
        "difficult":
        {
            0: "You clamber over the debris in your way.\n",
            1: "A piece of a long forgotten face stares at you as you scale the shattered marble.\n",
            2: "You crawl through the gnarled and twisted roots.\n",
            3: "You climb over stone and duck under a splintered beam and just barely manage to avoid impaling yourself on a broken pipe.\n",
            4: "Lower yourself carefully into the hole as much as you can, then slide the last few feet. Finding hand and foot holds between the stones of the wall you scale up the other side.\n"

        },
        "exhausted": "You die of thirst while trying to climb.\n",
        "travel": "You go {0}.\n"
    },
    "look":
    {
        "bad_input": "I'm sorry, I don't know that direction. Please use:\n{0}",
        "no_exit": "There is no exit that direction.\n",
        "success": "You look {0}:"
    },
    "say": "You say: {0}",
    "description":
    {
        "thirst":
        {
            0: "You are feeling pretty good all things considered.\n",
            1: "You feel good, though a little thirsty.\n",
            2: "Your mouth is dry.\n",
            3: "Your breath catches in your throat as you breathe. Your lips are chapped.\n",
            4: "Your eyelids stick when you blink. Your lips are cracked and bleeding.\n"
        },
        "exits": "There {0} door(s)",
        "number_words":
        {
            0: "are no",
            1: "is one",
            2: "are two",
            3: "are three",
            4: "are four"
        },
        "direction_words":
        {
            0: "in front of you",
            1: "to your right",
            2: "behind you",
            3: "to your left"
        },
    },
    "distant_description": "You hear softly echoing footsteps"

},
"Monster":
    {
        "kill": '\nYou feel teeth sink into your neck and shoulder and claws slicing into your side.\n',
        "distant_description": "You hear echoing wheezing",
        "description": "You hear ragged breaths",
        "visual_description": "You see a flash of glowing yellow eyes as something darts into the shadows."
    }


}
