from endlessdungeon.characters.Monster import Monster

from pubsub import pub


class ScoreTracker:
    # It may be worthwhile to create a stronger association between the value and it's multiplier
    room_multiplier = 1000
    monster_multiplier = 1000
    vials_multiplier = 100
    exertion_multiplier = 50
    moves_multiplier = 1
    escaped_multiplier = 100000

    def _escaped(self):
        self.escaped = True

    def _moved(self, character):
        if self.character == character:
            self.moves += 1

    def _exerted(self, character):
        if self.character == character:
            self.times_exerted += 1

    def _drank(self, character):
        if self.character == character:
            self.vials_drank += 1

    def _explored(self, character):
        if self.character == character:
            self.rooms_explored += 1

    def _slain(self, character):
        if isinstance(character, Monster):
            self.monsters_slain += 1

    def __init__(self, character):
        self.character = character
        self.times_exerted = 0
        self.vials_drank = 0
        self.rooms_explored = 0
        self.moves = 0
        self.monsters_slain = 0
        self.escaped = False
        pub.subscribe(self._moved, 'moved')
        pub.subscribe(self._drank, 'drank')
        pub.subscribe(self._exerted, 'exerted')
        pub.subscribe(self._explored, 'explored')
        pub.subscribe(self._slain, 'died')
        pub.subscribe(self._escaped, 'End.escape')


    @property
    def score(self):
        result = self.moves * ScoreTracker.moves_multiplier
        result += self.rooms_explored * ScoreTracker.room_multiplier
        result += self.vials_drank * ScoreTracker.vials_multiplier
        result += self.times_exerted * ScoreTracker.exertion_multiplier
        result += self.monsters_slain * ScoreTracker.monster_multiplier
        result += self.escaped * ScoreTracker.escaped_multiplier
        return result

#  TODO: move this shit
    def print_score(self):
        result = "Score:\n"
        length_to_end = 46

        result += ScoreTracker._format_line("Escaped:", self.escaped, ScoreTracker.escaped_multiplier)
        result += ScoreTracker._format_line("Rooms Explored:", self.rooms_explored, ScoreTracker.room_multiplier)
        result += ScoreTracker._format_line("Monsters Slain:", self.monsters_slain, ScoreTracker.monster_multiplier)
        result += ScoreTracker._format_line("Vials Drank:", self.vials_drank, ScoreTracker.vials_multiplier)
        result += ScoreTracker._format_line("Piles of Rubble Climbed:", self.times_exerted,
                                            ScoreTracker.exertion_multiplier)
        result += ScoreTracker._format_line("Moves Made:", self.moves, ScoreTracker.moves_multiplier)

        result += "_" * length_to_end + '\n'

        line = "Total:"
        line += str(self.score).rjust(length_to_end-len(line), ' ')

        result += line + '\n'

        return result

    @staticmethod  # pragma: no mutate
    def _format_line(text, value, multiplier):
        length_to_end_of_number = 27
        length_to_end_of_multiplier = 37
        length_to_end = 46

        line = text
        line += str(value).rjust(length_to_end_of_number-len(line), ' ')
        line += " x "
        line += str(multiplier).rjust(length_to_end_of_multiplier-len(line), ' ')
        line += " ="
        line += str(multiplier*value).rjust(length_to_end-len(line), ' ')
        return line+'\n'
