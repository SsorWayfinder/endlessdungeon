import random
import copy
from pubsub import pub

from endlessdungeon.items.Vial import Vial
from endlessdungeon.items.Candle import Candle
from endlessdungeon.CardinalDirection import CardinalDirection


def create(x, y):
    if Room.room_grid.keys().__contains__(x) and Room.room_grid[x].keys().__contains__(y):
        return Room.room_grid[x][y]
    else:
        return Room(x, y)


class Room:
    room_grid = dict()

    def _update_grid(self):
        if Room.room_grid.keys().__contains__(self.x):
            Room.room_grid[self.x][self.y] = self
        else:
            Room.room_grid[self.x] = {self.y: self}

    def __init__(self, x, y):
        self._characters = list()
        self.is_difficult = False
        self.difficult_type = 0
        self.items = list()
        self.exits = list()
        self.has_been_generated = False
        self.explored = list()
        self.x = x
        self.y = y
        self._update_grid()

    def is_lit(self):
        is_lit = self.x == 0 and self.y == 0  # pragma: no mutate
        items_to_check = self.items.copy()
        for character in self._characters:
            items_to_check += character.inventory.copy()
        for item in items_to_check:
            if isinstance(item, Candle):
                is_lit |= item.is_lit
        return is_lit

    def generate(self):
        if self.has_been_generated:
            return

        self._generate_exits()

        if random.randint(0, 79) == 0:
            self.items.append(Vial())

        if random.randint(0, 19) == 0:
            self.items.append(Candle())

        self.is_difficult = random.randint(0, 9) < 4
        if self.is_difficult:
            self.difficult_type = random.randint(0, 4)  # TODO: clean this up

        self.has_been_generated = True

    def _generate_exits(self):
        number_of_exits = len(self.exits)
        for direction in CardinalDirection:
            new_x = self.x + direction.x
            new_y = self.y + direction.y
            if not (Room.room_grid.keys().__contains__(new_x)
                    and Room.room_grid[new_x].keys().__contains__(new_y)):
                if number_of_exits <= 1 or random.randint(0, number_of_exits) == 0:
                    new_room = create(new_x, new_y)
                    new_room.exits.append(self)
                    self.exits.append(new_room)
                    number_of_exits += 1
            elif not Room.room_grid[new_x][new_y].has_been_generated:
                if random.randint(0, number_of_exits) == 0:
                    Room.room_grid[new_x][new_y].exits.append(self)
                    self.exits.append(Room.room_grid[new_x][new_y])
                    number_of_exits += 1

    def get_exit(self, direction: CardinalDirection):
        result = list(exit_room for exit_room in self.exits
                      if exit_room is not None and
                      (exit_room.x - self.x) == direction.x
                      and (exit_room.y - self.y) == direction.y)
        if len(result) > 0:
            return result[0]
        return None

    def add_character(self, character):
        if not self._characters.__contains__(character):
            self._characters.append(character)
        if character.current_room != self:  # pragma: no mutate #this times out
            character.current_room = self
        if character not in self.explored:
            pub.sendMessage("explored", character=character)
            self.explored.append(character)

    def remove_character(self, character):
        if self._characters.__contains__(character):
            self._characters.remove(character)

    def get_characters(self):
        return copy.copy(self._characters)
