import os
import sys

from pubsub import pub

from endlessdungeon import Command, ResourceManager


class UserInterface:

    def __init__(self, player, tracker):
        self.player = player
        self.tracker = tracker
        self.user_input = ""
        self.resource_manager = ResourceManager.ResourceManager()
        pub.subscribe(self.describe_room, "Character.Room.description")
        pub.subscribe(self.drop, "Character.drop")
        pub.subscribe(self.drink, "Character.drink")
        pub.subscribe(self.grab, "Character.grab")
        pub.subscribe(self.light, "Character.light")
        pub.subscribe(self.extinguish, "Character.extinguish")
        pub.subscribe(self.inventory, "Character.inventory")
        pub.subscribe(self.move, "Character.move")
        pub.subscribe(self.look, "Character.look")
        pub.subscribe(self.say, "say")
        pub.subscribe(self.thirst, "Character.description.thirst")
        pub.subscribe(self.exits, "Character.description.exits")
        pub.subscribe(self.character_distant_description, "Character.distant_description")
        pub.subscribe(self.monster_kill, "Monster.kill")
        pub.subscribe(self.monster_distant_description, "Monster.distant_description")
        pub.subscribe(self.monster_description, "Monster.description")
        pub.subscribe(self.describe_teleport_word, "Teleport.engraving")
        pub.subscribe(self.describe_teleport, "Teleport.teleport")
        pub.subscribe(self.describe_end, "End.description")
        pub.subscribe(self.escape_end, "End.escape")
        pub.subscribe(self.fountain_describe, "Fountain.description")
        pub.subscribe(self.fountain_refill, "Fountain.refill")

    @staticmethod  # TODO remove
    def resource_path(relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            # noinspection PyProtectedMember,PyUnresolvedReferences
            base_path = sys._MEIPASS
        except AttributeError:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)

    def intro(self):
        print(self.resource_manager.strings["intro"])

    def support(self):
        print(self.resource_manager.strings["support"])

    def quit_game(self):
        result = self.tracker.print_score()
        print(self.resource_manager.strings["quit"].format(result))
        pub.sendMessage('quit')

    def credits_function(self):
        print(self.resource_manager.strings["credits"].format(self.resource_manager.supporters))

    def help_function(self):
        result = ""
        for command in Command.Command:
            result += "\n" + self.resource_manager.strings["command"][command.name]
        print(result + "\n")

    def unknown_function(self):
        print(self.resource_manager.strings["unknown"])

    command_functions = \
        {
            Command.Command.SUPPORT: support,
            Command.Command.QUIT: quit_game,
            Command.Command.CREDITS: credits_function,
            Command.Command.HELP: help_function,
            Command.Command.UNKNOWN: unknown_function
        }

    def drop(self, state, items=""):
        if isinstance(items, str):
            print(self.resource_manager.strings["Character"]["drop"][state].format(items))
        else:
            item_description = ""
            for item in items:
                item_description += self.resource_manager.strings[type(item).__name__][item.state()] + "\n"
            print(self.resource_manager.strings["Character"]["drop"][state].format(item_description))

    def drink(self, state):
        print(self.resource_manager.strings["Character"]["drink"][state])

    def grab(self, state, item=""):
        if isinstance(item, str):
            print(self.resource_manager.strings["Character"]["grab"][state].format(item))
        else:
            item_description = ""
            for thing in item:
                item_description += self.resource_manager.strings[type(thing).__name__][thing.state()] + "\n"
            print(self.resource_manager.strings["Character"]["grab"][state].format(item_description))

    def light(self, state, position="", item=None):
        if item is None:
            print(self.resource_manager.strings["Character"]["light"][state].format(position))
        else:
            item_description = self.resource_manager.strings[type(item).__name__][item.state()]
            print(self.resource_manager.strings["Character"]["light"][state].format(position, item_description))

    def extinguish(self, state):
        print(self.resource_manager.strings["Character"]["extinguish"][state])

    def inventory(self, items):
        item_descriptions = ""
        index = 0
        for item in items:
            index += 1
            item_descriptions += str(index) + ": " + self.resource_manager.strings[type(item).__name__][item.state()] \
                                 + "\n"
        print(self.resource_manager.strings["Character"]["inventory"].format(item_descriptions))

    def move(self, state, direction=""):
        if len(direction) == 0:
            if state == "difficult":
                print(self.resource_manager.strings["Character"]["move"][state]
                      [self.player.current_room.difficult_type])
            else:
                print(self.resource_manager.strings["Character"]["move"][state])
        else:
            print(self.resource_manager.strings["Character"]["move"][state].format(direction.lower()))

    def look(self, state, direction=""):
        if state == "success":
            direction_looked = self.resource_manager.strings["Character"]["description"]["direction_words"][direction]
            print(self.resource_manager.strings["Character"]["look"][state]
                  .format(direction_looked))
        else:
            print(self.resource_manager.strings["Character"]["look"][state].format(direction.lower()))

    def thirst(self, state):
        print(self.resource_manager.strings["Character"]["description"]["thirst"][state])

    def exits(self, directions):
        result = self.resource_manager.strings["Character"]["description"]["exits"] \
            .format(self.resource_manager.strings["Character"]["description"]["number_words"][len(directions)])
        for direction in directions:
            result += ", " + self.resource_manager.strings["Character"]["description"]["direction_words"][direction]
        result += "."
        print(result)

    def character_distant_description(self, direction):
        print(self.resource_manager.strings["Character"]["distant_description"] + " " +
              self.resource_manager.strings["Character"]["description"]["direction_words"][direction])

    def monster_kill(self):
        print(self.resource_manager.strings["Monster"]["kill"])

    def monster_distant_description(self, direction):
        print(self.resource_manager.strings["Monster"]["distant_description"] + " " +
              self.resource_manager.strings["Character"]["description"]["direction_words"][direction])

    def monster_description(self, direction=None):
        if direction is not None:
            print(self.resource_manager.strings["Monster"]["description"] + " " +
                  self.resource_manager.strings["Character"]["description"]["direction_words"][direction])

    def describe_room(self, room):
        if not room.is_lit():
            print(self.resource_manager.strings["room"]["dark"])
        else:
            print(self.resource_manager.strings["room"]["lit"])

            if room.is_difficult:
                print(self.resource_manager.strings["room"]["difficult"][room.difficult_type])

            if len(room.items) > 0:
                print("\n" + self.resource_manager.strings["room"]["items"])
                for item in room.items:
                    print(self.resource_manager.strings[type(item).__name__][item.state()])
                print()

            if room.x == 0 and room.y == 0:
                print(self.resource_manager.strings["room"]["origin"])

            for character in room.get_characters():
                if character is not self.player:
                    print(self.resource_manager.strings[type(character).__name__]["visual_description"])
        print()

    def describe_player(self):
        self.player.description()

    def describe_teleport_word(self, word):
        print(self.resource_manager.strings["Teleport"]["engraving"].format(word))

    def describe_teleport(self):
        print(self.resource_manager.strings["Teleport"]["teleport"])

    def describe_end(self):
        print(self.resource_manager.strings["End"]["description"])

    def escape_end(self):
        print(self.resource_manager.strings["End"]["escape"])

    def fountain_describe(self):
        print(self.resource_manager.strings["Fountain"]["description"])

    def fountain_refill(self):
        print(self.resource_manager.strings["Fountain"]["refill"])

    def say(self, said, character):
        if character == self.player:
            print(self.resource_manager.strings["Character"]["say"].format(''.join(said)))

    def get_command(self):
        words = self.user_input.split()
        if len(words) == 0:
            return Command.Command.UNKNOWN
        if str.upper(words[0]) not in set(item.name for item in Command.Command):
            return Command.Command.UNKNOWN
        return Command.Command[str.upper(words[0])]

    def get_input(self):
        self.user_input = input(self.resource_manager.strings["input"])

    def get_arguments(self):
        words = self.user_input.split()
        if len(words) < 2:
            return list()
        else:
            return words[1:]
