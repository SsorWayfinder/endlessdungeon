import ast
import os
import sys


class ResourceManager:

    def __init__(self):

        with open(ResourceManager.resource_path("supporters.txt"), "r") as supporters_file:
            self.supporters = supporters_file.read()
        with open(ResourceManager.resource_path("english.rs"), "r") as english_file:
            self.strings = ast.literal_eval(english_file.read())

    @staticmethod
    def resource_path(relative_path):
        """ Get absolute path to resource, works for dev and for PyInstaller """
        try:
            # PyInstaller creates a temp folder and stores path in _MEIPASS
            # noinspection PyProtectedMember,PyUnresolvedReferences
            base_path = sys._MEIPASS
        except AttributeError:
            base_path = os.path.abspath(".")

        return os.path.join(base_path, relative_path)
