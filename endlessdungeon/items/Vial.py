from endlessdungeon.items.Item import Item


class Vial(Item):

    def __init__(self):
        self.is_full = True

    def consume(self):
        if self.is_full:
            self.is_full = False
            return True
        return False

    def state(self):
        if self.is_full:
            return 0
        else:
            return 1


