from pubsub import pub

from endlessdungeon.items.Item import Item


class Candle(Item):

    def __init__(self):
        self._is_lit = False
        self.time_left = 30
        pub.subscribe(self._count_down, "action")

    @property
    def is_lit(self):
        return self._is_lit

    @is_lit.setter
    def is_lit(self, value):
        if self.time_left > 0 and value:
            self._is_lit = True
        else:
            self._is_lit = False

    def _count_down(self):
        if self._is_lit:
            self.time_left -= 1
            if self.time_left <= 0:
                self._is_lit = False

    def state(self):
        if self.time_left <= 0:
            return 3
        if not self._is_lit:
            if self.time_left < 30:
                return 2
            else:
                return 1
        else:
            return 0
