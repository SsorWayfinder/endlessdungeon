from enum import Enum


class RelativeDirection(Enum):
    FORWARD = 0
    RIGHT = 1
    BACK = 2
    LEFT = 3
