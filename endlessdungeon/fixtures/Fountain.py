from pubsub import pub


from endlessdungeon.fixtures.Fixture import Fixture
from endlessdungeon.items.Vial import Vial


class Fountain(Fixture):

    def __init__(self, x, y):
        pub.subscribe(self.refill, "refill")
        super().__init__(x, y)

    def describe(self, room):
        if room is self.room:
            pub.sendMessage("Fountain.description")

    def refill(self, character):
        if character.current_room is self.room:
            for item in character.inventory:
                if isinstance(item, Vial):
                    item.is_full = True
            pub.sendMessage("Fountain.refill")
