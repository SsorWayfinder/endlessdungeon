from abc import ABCMeta

from pubsub import pub

from endlessdungeon import Room


class Fixture(metaclass=ABCMeta):
    def __init__(self, x, y):
        pub.subscribe(self.describe, "Character.Room.description")
        self.room = Room.create(x, y)

    def describe(self, room):
        pass
