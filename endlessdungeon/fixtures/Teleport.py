import random
import string
from pubsub import pub

from endlessdungeon.fixtures.Fixture import Fixture

from endlessdungeon import Room


class Teleport(Fixture):

    def __init__(self):
        x = random.randint(-10, 10)
        y = random.randint(-10, 10)
        letters = random.randint(5, 10)
        self.text = ''.join(random.choices(string.ascii_lowercase, k=letters))
        pub.subscribe(self.teleport, "say")
        super().__init__(x, y)

    def teleport(self, said, character):
        if said.__contains__(self.text):
            delta_x = random.randint(-100, 100)
            delta_y = random.randint(-100, 100)
            x = character.current_room.x + delta_x
            y = character.current_room.y + delta_y
            character.current_room = Room.create(x, y)
            character.current_room.generate()
            character.exert()  # TODO: would like this to just be thirst because this will count as traversing a difficult room
            pub.sendMessage("Teleport.teleport")
            pub.sendMessage("Character.Room.description", room=character.current_room)  # TODO: this is not the way to do this

    def describe(self, room):
        if room is self.room:
            pub.sendMessage("Teleport.engraving", word=self.text)

