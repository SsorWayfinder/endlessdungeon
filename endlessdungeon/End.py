import random
from pubsub import pub

from endlessdungeon import Room

minimum_distance = 25


class End:

    def __init__(self):
        x_direction = random.choice([True, False])
        y_direction = random.choice([True, False])

        if x_direction:
            x = random.randint(0, 100) + minimum_distance
        else:
            x = random.randint(-100, 0) - minimum_distance

        if y_direction:
            y = random.randint(0, 100) + minimum_distance
        else:
            y = random.randint(-100, 0) - minimum_distance

        self.room = Room.create(x, y)
        pub.subscribe(self.describe, "Character.Room.description")
        pub.subscribe(self.escape, "escape")

    def describe(self, room):
        if room is self.room:
            pub.sendMessage("End.description")

    def escape(self, character):
        if character.current_room is self.room:
            pub.sendMessage("End.escape")
            pub.sendMessage("quit")
        else:
            pub.sendMessage("Character.move", state="no_exit")  # TODO: fix this to avoid monster generated messages

