from endlessdungeon.Command import Command
from endlessdungeon import Room
from endlessdungeon.CardinalDirection import CardinalDirection
from endlessdungeon.items.Candle import Candle
from endlessdungeon.items.Vial import Vial
from endlessdungeon.RelativeDirection import RelativeDirection

from collections import deque
from pubsub import pub


class Character:

    def __init__(self, x=0, y=0):
        self.inventory = list()
        candle = Candle()
        candle._is_lit = True
        self.inventory.append(candle)

        self.cardinal_mapping = deque()
        self.cardinal_mapping.append(CardinalDirection.PY)
        self.cardinal_mapping.append(CardinalDirection.PX)
        self.cardinal_mapping.append(CardinalDirection.NY)
        self.cardinal_mapping.append(CardinalDirection.NX)
        self._current_room = None
        self.current_room = Room.create(x, y)
        self.current_room.generate()
        self.thirst = 0
        self._is_alive = True

    @property
    def is_alive(self):
        return self._is_alive

    @is_alive.setter
    def is_alive(self, value):
        self._is_alive = value
        if value is False:
            pub.sendMessage('died', character=self)

    @property
    def current_room(self):
        return self._current_room

    @current_room.setter
    def current_room(self, room):
        if self.current_room is not None:
            self.current_room.remove_character(self)
        self._current_room = room  # pragma: no mutate #this times out
        room.add_character(self)

    @property
    def orientation(self):
        return self.cardinal_mapping[0]

    def get_full_vials(self):
        full_vials = list()
        if self.current_room.is_lit():
            for item in self.current_room.items:
                if isinstance(item, Vial):
                    if item.is_full:
                        full_vials.append(item)
        for item in self.inventory:
            if isinstance(item, Vial):
                if item.is_full:
                    full_vials.append(item)

        return full_vials

    def drink(self, value=None):
        full_vials = self.get_full_vials()

        if len(full_vials) == 0:
            pub.sendMessage(type(self).__name__ + ".drink", state="nothing_to_drink")
            return

        if self.thirst == 0:
            pub.sendMessage(type(self).__name__ + ".drink", state="not_thirsty")
            return

        if self.is_alive:
            self.thirst -= 1
            full_vials[0].consume()
            pub.sendMessage('drank', character=self)  # TODO: change this to use the ui events?
            pub.sendMessage(type(self).__name__ + ".drink", state="success")

    def exert(self):
        if self.is_alive:
            pub.sendMessage('exerted', character=self)
            self.thirst += 1
        if self.thirst > 4:
            self.is_alive = False

    def move(self, value):
        direction = value[0].upper()
        if direction == "UP":
            pub.sendMessage('escape', character=self)
            return
        if direction not in RelativeDirection.__members__.keys():
            result = ''
            for direction_item in RelativeDirection:
                result += direction_item.name + '\n'
            result += "UP\n"
            pub.sendMessage(type(self).__name__ + ".move", state="bad_input", direction=result)
            return
        direction_value = RelativeDirection[direction].value
        cardinal_direction = self.cardinal_mapping[direction_value]
        exit_attempt = self.current_room.get_exit(cardinal_direction)
        if exit_attempt is None:
            pub.sendMessage(type(self).__name__ + ".move", state="no_exit")
            return
        if self.current_room.is_difficult and str.upper(direction) != "BACK":
            pub.sendMessage(type(self).__name__ + ".move", state="difficult")
            self.exert()
            if not self.is_alive:
                pub.sendMessage(type(self).__name__ + ".move", state="exhausted")
                return
        self.current_room = exit_attempt
        self.cardinal_mapping.rotate(4 - direction_value)
        self.current_room.generate()
        pub.sendMessage(type(self).__name__ + ".move", state="travel", direction=direction)
        pub.sendMessage(type(self).__name__ + ".Room.description", room=exit_attempt)
        pub.sendMessage('moved', character=self)

    def grab(self, values=None):
        if values is None:
            values = list()
        if len(self.current_room.items) > 0:
            picked_up = list()
            if len(values) == 0:
                if len(self.current_room.items) == 1:
                    picked_up.append(self.current_room.items[0])
                else:
                    pub.sendMessage(type(self).__name__ + ".grab", state="undefined")
                    return
            values = set(values)
            for value in values:
                values.remove(value)
                values.add(str.upper(value))
            if values.__contains__("ALL"):
                picked_up = self.current_room.items.copy()
            else:
                for position in values:
                    if position.isdigit():
                        position_number = int(position)
                        if position_number > len(self.current_room.items):
                            pub.sendMessage(type(self).__name__ + ".grab", state="no_item")
                            return

                        picked_up.append(self.current_room.items[position_number - 1])
            for item in picked_up:
                self.inventory.append(item)
                self.current_room.items.remove(item)
            pub.sendMessage(type(self).__name__ + ".grab", state="grabbed", item=picked_up)
            return
        else:
            pub.sendMessage(type(self).__name__ + ".grab", state="empty")

    def inventory_check(self, value=None):
        pub.sendMessage(type(self).__name__ + ".inventory", items=self.inventory)

    def drop(self, values):
        if len(self.inventory) == 0:
            pub.sendMessage(type(self).__name__ + ".drop", state="empty")
            return
        dropped = list()
        if len(values) == 0:
            if len(self.inventory) == 1:
                dropped.append(self.inventory[0])
            else:
                pub.sendMessage(type(self).__name__ + ".drop", state="undefined")
                self.inventory_check()
                return
        values = set(values)
        for value in values:
            values.remove(value)
            values.add(str.upper(value))
        if values.__contains__("ALL"):
            dropped = self.inventory.copy()
        else:
            for position in values:
                if position.isdigit():
                    position_number = int(position)
                    if position_number > len(self.inventory):
                        pub.sendMessage(type(self).__name__ + ".drop", state="no_item")
                        return

                    dropped.append(self.inventory[position_number - 1])

        for item in dropped:
            self.current_room.items.append(item)
            self.inventory.remove(item)
        pub.sendMessage(type(self).__name__ + ".drop", state="dropped", items=dropped)

    def light(self, values=None):
        if len(values) > 0:
            for value in values:
                if not value.isdigit():
                    pub.sendMessage(type(self).__name__ + ".light", state="bad_input", position=value)
                    continue
                position = int(value) - 1
                if position >= len(self.inventory) or position < 0:
                    pub.sendMessage(type(self).__name__ + ".light", state="no_item", position=value)
                    continue
                if not isinstance(self.inventory[position], Candle):
                    pub.sendMessage(type(self).__name__ + ".light", state="not_candle", position=value,
                                    item=self.inventory[position])
                    continue
                self.inventory[position].is_lit = True
                pub.sendMessage(type(self).__name__ + ".light", state="success_position", position=value)
        else:
            #  It may make sense to move this to it's own function
            candles_unlit_candles = list()
            for item in self.inventory:
                if not isinstance(item, Candle):
                    continue
                if not item.is_lit:
                    candles_unlit_candles.append(item)

            if len(candles_unlit_candles) < 1:
                pub.sendMessage(type(self).__name__ + ".light", state="no_candle")
                return
            if len(candles_unlit_candles) > 1:
                pub.sendMessage(type(self).__name__ + ".light", state="undefined")
                return
            candles_unlit_candles[0].is_lit = True
            pub.sendMessage(type(self).__name__ + ".light", state="success")

    def extinguish(self, values):
        extinguished = False
        for item in self.inventory:
            if isinstance(item, Candle):
                extinguished |= item.is_lit
                item.is_lit = False  # pragma: no mutate
        if extinguished:
            pub.sendMessage(type(self).__name__ + ".extinguish", state="success")
            return
        pub.sendMessage(type(self).__name__ + ".extinguish", state="failure")

    def look(self, values):
        direction = values[0].upper()
        if direction not in RelativeDirection.__members__.keys():
            result = ''
            for direction_item in RelativeDirection:
                result += direction_item.name + '\n'
            pub.sendMessage(type(self).__name__ + ".look", state="bad_input", direction=result)
            return
        direction_value = RelativeDirection[str.upper(direction)].value
        cardinal_direction = self.cardinal_mapping[direction_value]
        exit_attempt = self.current_room.get_exit(cardinal_direction)
        if exit_attempt is None:
            pub.sendMessage(type(self).__name__ + ".look", state="no_exit")
            return
        else:
            exit_attempt.generate()
            pub.sendMessage(type(self).__name__ + ".look", state="success", direction=direction_value)
            pub.sendMessage(type(self).__name__ + ".Room.description", room=exit_attempt)

    def say(self, words):
        pub.sendMessage("say", said=words, character=self)

    def refill(self, values):
        pub.sendMessage("refill", character=self)

    command_functions = \
        {
            Command.MOVE: move,
            Command.DRINK: drink,
            Command.GRAB: grab,
            Command.INVENTORY: inventory_check,
            Command.DROP: drop,
            Command.LIGHT: light,
            Command.EXTINGUISH: extinguish,
            Command.LOOK: look,
            Command.SAY: say,
            Command.REFILL: refill
        }

    def command(self, command, values):
        if len(values) > 0:
            if str.upper(values[0]) in set(item.name for item in Command):
                values.remove(values[0])
        return self.command_functions[command](self, values)

    def description(self):
        pub.sendMessage(type(self).__name__ + ".description.thirst", state=self.thirst)
        if self.current_room.is_lit():  # TODO: this logic needs to change to be if the current room is lit or the next
            self._door_description()
        self._sounds_description()

    def _door_description(self):
        resulting_directions = list()
        for direction in CardinalDirection:
            direction_exit = self.current_room.get_exit(direction=direction)
            if direction_exit is not None:
                resulting_directions.append(self.cardinal_mapping.index(direction))
        pub.sendMessage(type(self).__name__ + ".description.exits", directions=resulting_directions)

    def _sounds_description(self):
        for direction in CardinalDirection:
            direction_exit = self.current_room.get_exit(direction=direction)
            if direction_exit is not None:
                for character in direction_exit.get_characters():
                    pub.sendMessage(type(character).__name__ + ".description", direction=self.cardinal_mapping
                                    .index(direction))
                for room in direction_exit.exits:
                    if room != self.current_room:
                        for character in room.get_characters():
                            pub.sendMessage(type(character).__name__ + ".distant_description", direction=self
                                            .cardinal_mapping.index(direction))
