import endlessdungeon.characters.Character
from endlessdungeon import Room
from endlessdungeon.items.Candle import Candle
from endlessdungeon.RelativeDirection import RelativeDirection

import random
from pubsub import pub


def _pick_action(possible_actions):
    weights = list()
    for action in possible_actions:
        weights.append(action[2])
    choice = random.choices(possible_actions, weights=weights)
    choice[0][0](choice[0][1])


def _pick_x_y_deltas(x, y):
    delta_x = 0
    delta_y = 0
    while delta_x == 0:
        delta_x = random.randint(-2, 2)
    while delta_y == 0:
        delta_y = random.randint(-2, 2)
    return delta_x, delta_y


class Monster(endlessdungeon.characters.Character.Character):

    def __init__(self, x, y):
        deltas = _pick_x_y_deltas(x, y)
        super().__init__(x + deltas[0], y + deltas[1])
        while len(self.current_room.exits) == 0:
            deltas = _pick_x_y_deltas(x, y)
            self.current_room = Room.create(x + deltas[0], y + deltas[1])
        pub.subscribe(self._drop_inventory, "died")
        self.heard_player = list()
        pub.subscribe(self._heard, "Character.distant_description")

    def _heard(self, direction):
        if direction is not None:
            self.heard_player.append(RelativeDirection(direction))

    def _drop_inventory(self, character):
        if character is self:
            self.current_room.items = self.current_room.items + self.inventory

    def act(self):
        self.kill_other_characters()
        self.heard_player = list()
        self.description()
        possible_actions = self._list_possible_actions()
        _pick_action(possible_actions)
        self.kill_other_characters()

    def kill_other_characters(self):
        characters = self.current_room.get_characters()
        for character in characters:
            if character != self:
                character.is_alive = False
                pub.sendMessage(type(self).__name__ + ".kill")  # TODO: probably this should include a reference
                # to the character killed to allow for other npcs to be killed by the monster

    def _list_possible_actions(self):
        result = list()
        full_vials = self.get_full_vials()
        if len(full_vials) != 0:
            result.append([self.drink, [None], self.thirst*2])

        for direction in RelativeDirection:
            if self.current_room.get_exit(self.cardinal_mapping[direction.value]) is not None\
                    or not self.current_room.is_lit():
                weight = 1
                if self.heard_player.__contains__(direction):
                    weight += 10
                if direction is RelativeDirection.BACK and self.current_room.is_difficult:
                    weight += self.thirst*2
                result.append([self.move, [direction.name], weight])

        if not self.current_room.is_lit():
            result.append([self.grab, ["all"], 3])
            for item in self.inventory:
                if isinstance(item, Candle):
                    result.append([self.light, [str(self.inventory.index(item) + 1)], 3])
        else:
            for item in self.current_room.items:
                result.append([self.grab, [str(self.current_room.items.index(item) + 1)], self.thirst*2])
        return result
