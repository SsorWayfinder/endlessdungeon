from enum import Enum


class Command(Enum):

    def __init__(self, index: int, character_command: bool = False):
        self.index = index
        self.character_command = character_command

    UNKNOWN = 0
    HELP = 1
    SUPPORT = 2
    CREDITS = 3
    QUIT = 4
    MOVE = 5, True
    DRINK = 6, True
    GRAB = 7, True
    INVENTORY = 8, True
    DROP = 9, True
    LIGHT = 10, True
    EXTINGUISH = 11, True
    LOOK = 12, True
    SAY = 13, True
    REFILL = 14, True
