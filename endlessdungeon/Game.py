from pubsub import pub
import random

from endlessdungeon import Command
from endlessdungeon.UserInterface import UserInterface
from endlessdungeon.characters import Character
from endlessdungeon.ScoreTracker import ScoreTracker
from endlessdungeon.characters.Monster import Monster
from endlessdungeon.fixtures.Teleport import Teleport
from endlessdungeon.fixtures.Fountain import Fountain

from endlessdungeon.fixtures.End import End


class Game:

    def __init__(self):
        self.player_character = Character.Character()
        self.tracker = ScoreTracker(self.player_character)
        self.npcs = dict()
        self.npcs["monster"] = Monster(0, 0)
        self.fountains = list()
        pub.subscribe(self.spawn_monster, "died")
        self.ui = UserInterface(self.player_character, self.tracker)
        self.teleport = Teleport()
        self.end = End()
        self.quit = False
        pub.subscribe(self.set_quit, "quit")
        pub.subscribe(self.set_quit, "died")
        pub.subscribe(self.create_fountain, "explored")

    def set_quit(self, character=None):
        if character is None or character is self.player_character:
            self.quit = True

    def spawn_monster(self, character=None):
        if isinstance(character, Monster):
            self.npcs["monster"] = Monster(self.player_character.current_room.x,
                                           self.player_character.current_room.y)

    def create_fountain(self, character):
        room = character.current_room
        if len(room.explored) == 0:
            if random.randint(0, 99) == 0:
                self.fountains.append(Fountain(room.x, room.y))

    def main(self):
        self.ui.intro()
        pub.sendMessage("Character.Room.description", room=self.player_character.current_room)
        while not self.quit:
            self.ui.describe_player()
            self.ui.get_input()
            command = self.ui.get_command()
            if command.character_command:
                self.player_character.command(command, self.ui.get_arguments())
                self.npcs["monster"].act()
                pub.sendMessage("action")
            elif command is not Command.Command.QUIT:
                self.ui.command_functions[command](self.ui)
            else:
                break
        self.ui.command_functions[Command.Command.QUIT](self.ui)


if __name__ == '__main__':  # pragma: no mutate
    Game().main()  # pragma: no mutate
