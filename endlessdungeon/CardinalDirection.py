from enum import Enum


class CardinalDirection(Enum):

    def __init__(self, index: int, x, y):
        self.index = index
        self.x = x
        self.y = y

    NX = 0, -1, 0
    PY = 1, 0, 1
    PX = 2, 1, 0
    NY = 3, 0, -1
