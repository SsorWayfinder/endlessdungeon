# v0.14.0

# Intelligent Monster

I have added weights to the choices the monster can make based on if it hears you and how thirsty it is.

It cannot properly interact with fountains at the moment.

# Other

A monster slain is worth ~~500~~1000 points.

# V0.13.0


## Fountains
Added Fountains that will randomly spawn in rooms at a `1%` chance.
They have the description of:

> A small marble fountain quietly bubbles in the middle of the room.

You can drink from them as much as you want, _and_...

## Refill

Added command `refill`. Currently, only works in a room with a fountain. Causes all the vials in your inventory to become full.

## Other
* To balance the existence of fountains, vials now have a ~~1/40~~1/80 chance of spawning in a new room. 

This may end up being too extreme, but also being able to completely remove your thirst, and refill a potentially unlimited number of vials could be game breaking. 
This will hopefully at least limit the frequency of broken games.

### Bugfixes
* Fixed a bug where the End added in the last release wasn't actually being generated.


# V0.12.0


## An End to the Endless Dungeon

There is now a room you can escape through at least 25 rooms away. It includes the description:
> A spiralling staircase extends up into a faint light.

This is also now the only room in which you can move `up`.

Escaping adds to your score 100000.

# V0.11.0

## Magic Word:Teleport

Added a random room that spawns somewhere between -10 and 10 x and y. 
Said room has the added description of:
>You see engraved on the ground:

Followed by a randomly generated word.

New command added: `say`. If you say the engraved word, you are teleported up to 100 rooms away in any direction. 

## Other

Added `====================` before each input to more clearly show what happened since your last command.

# V0.10.0

## Looking Into Neighboring Rooms

Added a new command: `look` which tells you the description of a neighboring room when followed by a direction you could go. This counts as an action (the monster is able to move during this time).

As a result it is now possible to spot the monster, though that may not be the best use of your time.

## Other 

Rooms are now only described when you first enter them (or if you look into them). This should clean up the output a little and make it a little clearer what the result of your last command was.

Because you can only see in a room if it is lit, made the following adjustments:
* Candles last: ~~10~~ 30 actions
* Chance of a room containing a vial of water: ~~1/20(5%)~~ 1/40(2.5%) 
  * This is an attempt to balance. It may be changed in future releases.

# V0.9.0

## New Difficult Rooms

The following new descriptions for difficult rooms have been added:
```
        "Fragments of a what appears to have been the statue of an enormous figure litter the floor. Moving any direction but back will require you to exert yourself climbing over marble limbs."
        "The roots of a mighty tree fill the room. Climbing under and through the thick vegetation will be exhausting but necessary if you wish to go any direction but back."
        "The ceiling of this room has fallen in leaving piles of stone, pipes, and beams in your way if you don't want to go back.",
        "You stand just on the edge of a great pit. You will have to scale its sides if you want to pass through this room."
```
Along with the following descriptions for traversing them:
```
        "A piece of a long forgotten face stares at you as you scale the shattered marble."
        "You crawl through the gnarled and twisted roots."
        "You climb over stone and duck under a splintered beam and just barely manage to avoid impaling yourself on a broken pipe."
        "Lower yourself carefully into the hole as much as you can, then slide the last few feet. Finding hand and foot holds between the stones of the wall you scale up the other side."
```
## Other

There has been significant changes to how input and output are handled in the game. Things may now look slightly different. Feel free to give feedback (on patreon, discord, or via email @ minimumviablegame@gmail.com) on how things are displayed.

# V0.8.0

## Wasting Time

It is now the case that any **character command** gives 
the monster a chance to act (that may have been the case before?).

A **character command** is any of the of the following:
* `move`
* `drink`
* `grab`
* `inventory`
* `drop`
* `light`
* `extinguish`

Important to this change, this includes invalid commands
like moving in a direction that doesn't exist or grabbing
nothing.

## Darkness

Rooms without a light source now have the description:
> You are unable to make out any of the features of the room through the thick darkness.

The initial Room is lit by the grate in the ceiling.

## Candles

Candles keep the darkness at bay for 10 turns.

Rooms now have a 1/20(5%) chance to have a candle in them.

Added new commands:

`light` - Light a candle in your inventory. Followed by the number (position) of the candle.

`extinguish` - Put out all currently burning candles in your possession.

## Credits
Added to the Credits:

> Charleston Wall

Thank you for your support!

# V0.7.2

Monster now refuses to spawn in rooms without exits.

# V0.7.1

Once again runs as a module.

# V0.7

* Initial room no longer counts as explored

* Chance of a room containing a vial of water ~~1/40(2.5%)~~ 1/20(5%)
  
> _This is meant to balance the greater danger added, and to offset the added competition. This may not completely offset both of those things so there may be some tweaks in the next version._

## Monster

Created a monster that:
   * Spawns within a 5x5 grid around you
   * Also gets thirsty and dies same as you after traversing too many rubble filled rooms
     * If it dies a new monster spawns
     * You receive 500 points for each monster that dies while you explore the dungeon
   * Will kill you instantly if it enters the same room as you
   * It also can drink water, meaning you might encounter empty vials you did not drink...
   * You can hear it if is in the next room

# V0.6

Made the game wait for input to end, to give the player time to read the last messages.

## Score

Added a score that will be presented at the end of the game. The score is calculated by the following:
* 1000 points for rooms explored
* 100 points for vials drank
* 50 points for piles of rubble climbed
* 1 point for moves made

## Bugs
Fixed a bug where having an empty vial in the room or in your inventory kept you from drinking. 

# V0.5

* Chance of a room containing a vial of water ~~1/20(5%)~~ 1/40(2.5%)
* Room descriptions have changed slightly to account for the possibility of multiple vials in a room.

## Inventory

Added a character inventory allowing the player to take vials and drink from them wherever.

### Command: Grab
Added new command `grab x` that will place the xth vial into your inventory if there are at least x in the room.
 
### Command: Inventory
Added new command `inventory` that will list all the items in your inventory.

### Command: Drop
Added new command `drop x` that will drop the item in the x position in your inventory.

## Credits
Added to the Credits:

    Dalton Toberman
    
Thank you for your support!

# V0.4
## Room Updates
* Added a room grid to track the spacial relationship between rooms.
As a result:
  * You can now get back to a room by going in a circle (four lefts or four rights)
  * Exits/doors are no longer a random number 1-4 added to a room, 
  instead each direction checks if there is already a room there, then has a chance
  to add on if not (100% chance for the first, 100% for the second, 50% chance for the third
  and 33.3% chance for the last.) This will still result in a bias towards a particular door in the first room, but from there the doors should be mostly random.
* The first room's description now includes the grate that you were thrown through.

## Character Updates
* Character now has a (spacial) orientation.
* The "Character" object now describes the the doors instead of the "Room" object, slightly changing where that description is.

## UserInterface
* Initial message now mentions the "help" command.
* Some other formatting changes to the Initial message.

# V0.2
* Chance of a room being difficult ~~1/3(33.3%)~~ 2/5(40%)
* Chance of a room containing a vial of water ~~1/6(16.7%)~~ 1/20(5%)

## Addition of Character State Description
You will now get a status of your thirst with the following descriptions:
            
            0 thirst: "You are feeling pretty good all things considered."
           
            1 thirst: "You feel good, though a little thirsty."
            
            2 thirst: "Your mouth is dry."
            
            3 thirst: "Your breath catches in your throat as you breathe. Your lips are chapped."
            
            4 thirst (aka almost dead): "Your eyelids stick when you blink. Your lips are cracked and bleeding."

## Credits
Added to the credits:

    Max Petersen    
    Jo Lee
    Jelani Hurtault
   
Thank you for your support!
