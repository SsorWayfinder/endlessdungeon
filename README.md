# Running Game via Executable

Get the lasest execuable for:

[Windows](https://gitlab.com/SsorWayfinder/endlessdungeon/-/jobs/artifacts/master/raw/dist/EndlessDungeon.exe?job=win_Build)
[Linux](https://gitlab.com/SsorWayfinder/endlessdungeon/-/jobs/artifacts/master/raw/dist/EndlessDungeon?job=py372_Build)

# Running Game From Source 

If you do not have python or want to make sure you have the latest version:

*  Open the browser of your choice
* Go to  https://www.python.org/ 
* Click Downloads
* It should say "Download the latest version for " followed by your operating system
  * If it does not say your operating system, click on the link for your operating system under the "Download Python" button
* Click the "Download Python" button
* Once it has finished downloading, run the downloaded file
* If you are running Windows be sure to click the check box to add python to PATH at the beginning of the wizard



## To get the game

* You can download [here](https://gitlab.com/SsorWayfinder/endlessdungeon/-/archive/master/endlessdungeon-master.zip)
  * If you are interested in contributing to the code or want to keep up to date via git, you can clone [here](https://gitlab.com/SsorWayfinder/endlessdungeon.git) 
* If you download it, it will be a zip file which you will have to unzip
  * If you do not have a program for unzipping you can get 7zip at https://www.7-zip.org/

## To Play

* Open a terminal
  * On Windows, you can use Command Prompt or PowerShell
  * On Mac, you can use Terminal
  * On Linux, you can use xterm
* Navigate to the folder that you unzipped to or cloned to 
  * In most terminals you can do this by typing: `cd  `
  * Then add a space 
  * Then drag the folder you unzipped to onto the terminal
  * Then press enter
* Then type: `python -m endlessdungeon`
  * If that doesn't work try `python3 -m endlessdungeon`



If you have any questions, feel free to reach out to me at minimumviablegame@gmail.com



If you want to support development, I have a Patreon for this project: https://www.patreon.com/minimumviablegame