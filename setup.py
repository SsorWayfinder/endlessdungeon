from distutils.core import setup

setup(
    name='endlessdungeon',
    version='0.9.0',
    packages=['endlessdungeon', 'test'],
    url='https://gitlab.com/SsorWayfinder/endlessdungeon',
    license='GNU v3',
    author='Ross Mattingly',
    author_email='minimumviablegame@gmail.com',
    description='A very simple game'
)
