import unittest
from unittest import mock
from unittest.mock import MagicMock, patch
import os
import io

from pubsub import pub

from endlessdungeon.Game import Game
from endlessdungeon.UserInterface import UserInterface
from endlessdungeon.characters.Character import Character
from endlessdungeon.characters.Monster import Monster
from endlessdungeon.ScoreTracker import ScoreTracker
from endlessdungeon.Command import Command
from endlessdungeon.fixtures.Teleport import Teleport
from endlessdungeon.fixtures.End import End


class TestGame(unittest.TestCase):
    def setUp(self):
        self.mock_interface = MagicMock(spec=UserInterface)
        with patch('endlessdungeon.Game.UserInterface', self.mock_interface):
            self.game = Game()
            self.mock_monster = MagicMock(spec=Monster)
            self.mock_monster.act = MagicMock(return_value="")
            self.game.npcs["monster"] = self.mock_monster
            self.game.quit = False
            self.game.end = MagicMock(spec=End)
            self.game.fountains = list()

    @patch('endlessdungeon.Game.UserInterface')
    @patch('endlessdungeon.Game.Monster')
    def test_init(self, test_monster_namespace, test_interface_namespace):
        # arrange
        test_interface_namespace.return_value = self.mock_interface
        mock_monster = MagicMock(spec=Monster)
        test_monster_namespace.return_value = mock_monster
        # act
        game = Game()
        # assert
        test_monster_namespace.assert_called_once_with(0, 0)
        self.assertIsInstance(game.player_character, Character)
        self.assertEqual(game.npcs["monster"], mock_monster)
        self.assertIsInstance(game.tracker, ScoreTracker)
        self.assertIsInstance(game.ui, UserInterface)
        self.assertIsInstance(game.teleport, Teleport)
        self.assertIsInstance(game.end, End)
        self.assertEqual(False, game.quit)
        self.assertIsInstance(game.fountains, list)

    def test_monster_die_respawn(self):
        # act
        pub.sendMessage('died', character=self.mock_monster)
        # assert
        self.assertNotEqual(self.mock_monster, self.game.npcs["monster"])
        self.assertIsInstance(self.game.npcs["monster"], Monster)

    def test_set_quit(self):
        # arrange
        # act
        pub.sendMessage('quit')
        # assert
        self.assertTrue(self.game.quit)

    def test_create_fountain(self):
        # arrange
        mock_room = mock.MagicMock()
        mock_room.x = 0
        mock_room.y = 1
        mock_room.explored = list()
        mock_character = MagicMock(current_room=mock_room)
        mock_random = mock.Mock(return_value=0)
        with mock.patch('random.randint', mock_random):
            # act
            pub.sendMessage("explored", character=mock_character)
            # assert
            mock_random.assert_called_with(0, 99)
            self.assertEqual(1, len(self.game.fountains))
            self.assertEqual(0, self.game.fountains[0].room.x)
            self.assertEqual(1, self.game.fountains[0].room.y)

    def test_character_death_is_quit(self):
        # act
        pub.sendMessage('died', character=self.game.player_character)
        # assert
        self.assertTrue(self.game.quit)

    def test_main(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # arrange
            fake_ui = MagicMock()
            self.game.ui = fake_ui
            self.game.ui.intro = MagicMock(side_effect=print("intro"))
            self.game.ui.describe_room = MagicMock(side_effect=print("a room"))

            def side_effect():
                pub.sendMessage('quit')

            self.game.ui.get_input = MagicMock(side_effect=side_effect)
            self.game.ui.get_command = MagicMock(return_value=Command.QUIT)
            fake_func = MagicMock()
            self.game.ui.command_functions[Command.QUIT] = fake_func

            has_been_called2 = [False]
            rooms = list()

            def listener2(room):
                has_been_called2[0] = True
                rooms.append(room)

            pub.subscribe(listener2, "Character.Room.description")

            # act
            self.game.main()
            # assert
            self.assertTrue(has_been_called2[0])
            self.assertEqual(rooms[0], self.game.player_character.current_room)
            self.assertEqual("intro\na room\n", fake_stdout.getvalue())
            self.game.ui.get_input.assert_any_call()
            self.game.ui.command_functions[Command.QUIT].assert_called_with(fake_ui, )

    def test_main_character_command(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # arrange
            self.game.player_character = MagicMock()
            self.game.player_character.command = MagicMock()
            self.game.npcs["monster"] = MagicMock()
            self.game.npcs["monster"].act = MagicMock()
            fake_ui = MagicMock()
            self.game.ui = fake_ui
            self.game.ui.intro = MagicMock(side_effect=print("intro"))
            self.game.ui.describe_room = MagicMock(side_effect=print("a room"))

            def side_effect():
                pub.sendMessage('quit')

            self.game.ui.get_input = MagicMock(side_effect=side_effect)
            self.game.ui.get_command = MagicMock(return_value=Command.INVENTORY)
            fake_func = MagicMock()
            self.game.ui.command_functions[Command.QUIT] = fake_func
            has_been_called = [False]

            def listener():
                has_been_called[0] = True

            pub.subscribe(listener, 'action')
            # act
            self.game.main()
            # assert
            self.assertEqual("intro\na room\n", fake_stdout.getvalue())
            self.game.ui.get_input.assert_any_call()
            self.game.npcs["monster"].act.assert_called_once()
            self.game.player_character.command.assert_called_once()
            self.assertTrue(has_been_called[0])

    def test_main_function_command(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # arrange
            self.game.player_character = MagicMock()
            self.game.player_character.command = MagicMock()
            self.game.npcs["monster"] = MagicMock()
            self.game.npcs["monster"].act = MagicMock()
            fake_ui = MagicMock()
            self.game.ui = fake_ui
            self.game.ui.intro = MagicMock(side_effect=print("intro"))
            self.game.ui.describe_room = MagicMock(side_effect=print("a room"))

            def side_effect():
                pub.sendMessage('quit')

            self.game.ui.get_input = MagicMock(side_effect=side_effect)
            self.game.ui.get_command = MagicMock(return_value=Command.SUPPORT)
            self.game.ui.command_functions = dict()
            fake_func = MagicMock()
            self.game.ui.command_functions[Command.QUIT] = fake_func
            fake_support = MagicMock()
            self.game.ui.command_functions[Command.SUPPORT] = fake_support
            has_been_called = [False]

            def listener():
                has_been_called[0] = True

            pub.subscribe(listener, 'action')
            # act
            self.game.main()
            # assert
            self.assertEqual("intro\na room\n", fake_stdout.getvalue())
            self.game.ui.get_input.assert_any_call()
            self.game.npcs["monster"].act.assert_not_called()
            self.game.player_character.command.assert_not_called()
            self.assertEqual(False, has_been_called[0])
            self.assertTrue(self.game.ui.command_functions[Command.SUPPORT].called)

    def test_main_quit_command(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # arrange
            self.game.player_character = MagicMock()
            self.game.player_character.command = MagicMock()
            self.game.npcs["monster"] = MagicMock()
            self.game.npcs["monster"].act = MagicMock()
            fake_ui = MagicMock()
            self.game.ui = fake_ui
            self.game.ui.intro = MagicMock(side_effect=print("intro"))
            self.game.ui.describe_room = MagicMock(side_effect=print("a room"))
            self.game.ui.get_input = MagicMock()
            self.game.ui.get_command = MagicMock()
            self.game.ui.get_command.side_effect = [Command.QUIT, Command.SUPPORT]
            self.game.ui.command_functions = dict()

            def side_effect(ui):
                pub.sendMessage('quit')

            fake_func = MagicMock(side_effect=side_effect)
            self.game.ui.command_functions[Command.QUIT] = fake_func
            fake_support = MagicMock()
            self.game.ui.command_functions[Command.SUPPORT] = fake_support
            has_been_called = [False]

            def listener():
                has_been_called[0] = True

            pub.subscribe(listener, 'action')
            # act
            self.game.main()
            # assert
            self.assertEqual("intro\na room\n", fake_stdout.getvalue())
            self.game.ui.get_input.assert_any_call()
            self.game.npcs["monster"].act.assert_not_called()
            self.game.player_character.command.assert_not_called()
            self.assertEqual(False, has_been_called[0])
            self.assertFalse(self.game.ui.command_functions[Command.SUPPORT].called)


if __name__ == '__main__':
    unittest.main()
