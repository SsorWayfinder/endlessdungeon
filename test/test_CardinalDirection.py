import unittest
from endlessdungeon import CardinalDirection


class MyTestCase(unittest.TestCase):

    def test_index(self):
        index = 0
        for direction in CardinalDirection.CardinalDirection:
            with self.subTest():
                self.assertEqual(index, direction.index)
                index += 1


if __name__ == '__main__':
    unittest.main()
