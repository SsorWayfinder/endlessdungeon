import unittest
import unittest.mock as mock

from pubsub import pub

from endlessdungeon import End
from endlessdungeon import Room


class TestEnd(unittest.TestCase):

    def tearDown(self):
        Room.Room.room_grid = dict()

    test_data = [[25, True, 50, 0, 100], [-25, False, -50, -100, 0]]

    def test_init(self):
        for data in self.test_data:
            with self.subTest():
                # arrange
                mock_random_int = unittest.mock.Mock(return_value=data[0])
                mock_random_choice = unittest.mock.Mock(return_value=data[1])
                with mock.patch('random.randint', mock_random_int):
                    with mock.patch('random.choice', mock_random_choice):
                        # act
                        end = End.End()
                        # assert
                        self.assertEqual(data[2], end.room.x)
                        self.assertEqual(data[2], end.room.y)
                        mock_random_choice.assert_has_calls([unittest.mock.call([True, False]),
                                                             unittest.mock.call([True, False])])
                        mock_random_int.assert_has_calls([unittest.mock.call(data[3], data[4]),
                                                          unittest.mock.call(data[3], data[4])])

    def test_description(self):
        # arrange
        end = End.End()
        mock_room = unittest.mock.MagicMock()
        end.room = mock_room

        has_been_called = [False]

        def listener():
            has_been_called[0] = True

        pub.subscribe(listener, 'End.description')
        # act
        pub.sendMessage("Character.Room.description", room=mock_room)
        # assert
        self.assertTrue(has_been_called[0])

    def test_escape(self):
        # arrange
        end = End.End()
        mock_character = unittest.mock.MagicMock()
        mock_room = unittest.mock.MagicMock()
        end.room = mock_room
        mock_character.current_room = mock_room
        has_been_called = [False]

        def listener():
            has_been_called[0] = True

        has_been_called2 = [False]

        def listener2():
            has_been_called2[0] = True

        pub.subscribe(listener, "End.escape")
        pub.subscribe(listener2, "quit")
        # act
        pub.sendMessage("escape", character=mock_character)
        # assert
        self.assertTrue(has_been_called[0])
        self.assertTrue(has_been_called2[0])

    def test_escape_fail(self):
        # arrange
        end = End.End()
        mock_character = unittest.mock.MagicMock()
        mock_room = unittest.mock.MagicMock()
        end.room = mock_room
        mock_character.current_room = unittest.mock.MagicMock()
        has_been_called = [False]

        def listener():
            has_been_called[0] = True

        has_been_called2 = [False]

        def listener2():
            has_been_called2[0] = True

        has_been_called3 = [False]
        states = list()

        def listener3(state, direction=None):
            has_been_called3[0] = True
            states.append(state)

        pub.subscribe(listener3, "Character.move")
        pub.subscribe(listener, "End.escape")
        pub.subscribe(listener2, "quit")
        # act
        pub.sendMessage("escape", character=mock_character)
        # assert
        self.assertEqual(False, has_been_called[0])
        self.assertEqual(False, has_been_called2[0])
        self.assertTrue(has_been_called3)
        self.assertEqual("no_exit", states[0])

