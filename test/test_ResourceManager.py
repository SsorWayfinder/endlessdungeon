import os
import unittest

from endlessdungeon import ResourceManager
from unittest.mock import patch
from unittest.mock import mock_open


class MyTestCase(unittest.TestCase):

    def setUp(self):
        with patch("builtins.open", mock_open(read_data='{"test": "value"}')) as self.mock_file:
            self.resourceManager = ResourceManager.ResourceManager()

    def test_init(self):
        self.assertEqual('{"test": "value"}', self.resourceManager.supporters)
        self.assertEqual(self.resourceManager.strings["test"], "value")
        self.mock_file.assert_any_call(self.resourceManager.resource_path("supporters.txt"), "r")
        self.mock_file.assert_any_call(self.resourceManager.resource_path("english.rs"), "r")

    def test_resource_path(self):
        # arrange
        file_name = "test"
        # act
        result = self.resourceManager.resource_path(file_name)
        # assert
        self.assertEqual(os.path.join(os.path.abspath("."), file_name), result)


if __name__ == '__main__':
    unittest.main()
