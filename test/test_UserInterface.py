import os
import unittest
import io
from unittest.mock import patch, MagicMock, mock_open
from pubsub import pub

from endlessdungeon import UserInterface
from endlessdungeon.Command import Command
from endlessdungeon.items.Candle import Candle
from endlessdungeon.items.Vial import Vial
from endlessdungeon.characters.Monster import Monster
from endlessdungeon.characters.Character import Character


class TestUserInterface(unittest.TestCase):

    def setUp(self):
        self.mock_resources = MagicMock()
        with patch('endlessdungeon.UserInterface.ResourceManager', self.mock_resources):
            self.player = MagicMock()
            self.tracker = MagicMock()
            self.userInterface = UserInterface.UserInterface(self.player, self.tracker)

    def test_init(self):
        self.assertEqual(self.userInterface.user_input, "")
        self.assertEqual(
            {Command.SUPPORT: self.userInterface.support.__func__,
             Command.QUIT: self.userInterface.quit_game.__func__,
             Command.CREDITS: self.userInterface.credits_function.__func__,
             Command.HELP: self.userInterface.help_function.__func__,
             Command.UNKNOWN: self.userInterface.unknown_function.__func__
             },
            self.userInterface.command_functions)
        self.assertEqual(self.player, self.userInterface.player)

    def test_intro(self):
        # arrange
        self.userInterface.resource_manager.strings = {"intro": "INTRO!"}
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.intro()
            # assert
            self.assertEqual("INTRO!\n", fake_stdout.getvalue())

    def test_description_no_exits_not_difficult_no_vial(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone."}}
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n\n",
                             fake_stdout.getvalue())

    def test_description_room_monster(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone."},
             "Monster":
                 {"visual_description": "You see a flash of glowing yellow eyes as something darts into the shadows."}}
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        room.get_characters = MagicMock(return_value=[Monster(0, 0)])
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n"
                             "You see a flash of glowing yellow eyes as something darts into the shadows.\n\n",
                             fake_stdout.getvalue())

    def test_description_room_player_character(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone."},
             "Character":
                 {"visual_description": "This doesn't make any sense."}}
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()

        character = Character()
        self.userInterface.player = character
        room.get_characters = MagicMock(return_value=[character])
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n\n",
                             fake_stdout.getvalue())

    def test_description_no_exits_not_difficult_no_vial_lit_candle(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone.",
                      "items": "Sitting in the middle of the room there is:"},
             "Candle": {0: "A lit candle."}}
        self.userInterface.resource_manager = self.mock_resources

        candle = Candle()
        candle._is_lit = True
        room = MagicMock()
        room.x = 0
        room.y = 1
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        room.items.append(candle)
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n"
                             "\nSitting in the middle of the room there is:\n"
                             "A lit candle.\n\n\n",
                             fake_stdout.getvalue())

    def test_description_no_exits_difficult(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone.",
                      "difficult":
                          {
                              0: "The floor is covered in rubble. Moving any direction but back will require you to "
                                 "exert yourself climbing over jagged stone."}}}
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = True
        room.difficult_type = 0
        room.exits = list()
        room.items = list()
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone."
                             "\nThe floor is covered in rubble."
                             " Moving any direction but back will require you to exert "
                             "yourself climbing over jagged stone.\n\n",
                             fake_stdout.getvalue())

    def test_description_no_exits_not_difficult_with_empty_vial(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone.",
                      "items": "Sitting in the middle of the room there is:"},
             "Vial":
                 {0: "test",
                  1: "An empty vial."}}
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        vial = Vial()
        vial.is_full = False
        room.items.append(vial)
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n"
                             "\nSitting in the middle of the room there is:\n"
                             "An empty vial.\n\n\n",
                             fake_stdout.getvalue())

    def test_description_unused_candle(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone.",
                      "items": "Sitting in the middle of the room there is:"},
             "Candle": {0: "A lit candle.",
                        1: "An unlit, unused candle."}}
        self.userInterface.resource_manager = self.mock_resources

        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        room.items.append(Candle())
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n"
                             "\nSitting in the middle of the room there is:\n"
                             "An unlit, unused candle.\n\n\n",
                             fake_stdout.getvalue())

    def test_description_first_room(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone.",
                      "origin": "Dim light filters through a grate set in the ceiling."}}
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.x = 0
        room.y = 0
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone."
                             "\nDim light filters through a grate set in the ceiling.\n\n", fake_stdout.getvalue())

    def test_description_no_exits_not_difficult_with_many_full_vials(self):
        # arrange
        self.mock_resources.strings = \
            {
                "room":
                    {
                        "lit": "The walls and floor are all made of the same bland gray stone.",
                        "items": "Sitting in the middle of the room there is:"
                    },
                "Vial": {0: "A vial full of water."}
            }
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        room.items.append(Vial())
        room.items.append(Vial())
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n"
                             "\nSitting in the middle of the room there is:\n"
                             "A vial full of water.\n"
                             "A vial full of water.\n\n\n",
                             fake_stdout.getvalue())

    def test_description_no_exits_not_difficult_with_many_empty_vials(self):
        # arrange
        self.mock_resources.strings = \
            {"room": {"lit": "The walls and floor are all made of the same bland gray stone.",
                      "items": "Sitting in the middle of the room there is:"},
             "Vial":
                 {0: "test",
                  1: "An empty vial."}}
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        vial = Vial()
        vial.is_full = False
        room.items.append(vial)
        vial2 = Vial()
        vial2.is_full = False
        room.items.append(vial2)
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n"
                             "\nSitting in the middle of the room there is:\n"
                             "An empty vial.\n"
                             "An empty vial.\n\n\n",
                             fake_stdout.getvalue())

    def test_description_no_exits_not_difficult_with_many_both_vials(self):
        # arrange
        self.mock_resources.strings = \
            {
                "room":
                    {
                        "lit": "The walls and floor are all made of the same bland gray stone.",
                        "items": "Sitting in the middle of the room there is:"
                    },
                "Vial":
                    {
                        0: "A vial full of water.",
                        1: "An empty vial."
                    }

            }
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = False
        room.exits = list()
        room.items = list()
        room.items.append(Vial())
        vial2 = Vial()
        vial2.is_full = False
        room.items.append(vial2)
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("The walls and floor are all made of the same bland gray stone.\n"
                             "\nSitting in the middle of the room there is:\n"
                             "A vial full of water.\n"
                             "An empty vial.\n\n\n",
                             fake_stdout.getvalue())

    def test_description_dark(self):
        # arrange
        self.mock_resources.strings = \
            {
                "room":
                    {
                        "dark": "It's dark",
                        "items": "Sitting in the middle of the room there is:"
                    }
            }
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=False)
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.describe_room(room)
            # assert
            self.assertEqual("It's dark\n\n", fake_stdout.getvalue())

    def test_description_dark_event(self):
        # arrange
        self.mock_resources.strings = \
            {
                "room":
                    {
                        "dark": "It's dark",
                        "items": "Sitting in the middle of the room there is:"
                    }
            }
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=False)
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.Room.description", room=room)
            # assert
            self.assertEqual("It's dark\n\n", fake_stdout.getvalue())

    def test_get_input(self):
        # arrange
        self.mock_resources.strings = {"input": ":"}
        self.userInterface.resource_manager = self.mock_resources
        with patch('builtins.input', side_effect=["quit"]) as fake_input:
            # act
            self.userInterface.get_input()
            # assert
            self.assertEqual(self.userInterface.user_input, "quit")
            self.assertEqual(':', fake_input.call_args_list[0][0][0])

    def test_get_no_command(self):
        # arrange
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            self.userInterface.user_input = ''
            # act
            result = self.userInterface.get_command()
            # assert
            self.assertEqual("", fake_stdout.getvalue())
            self.assertEqual(result, Command.UNKNOWN)

    def test_get_bad_command(self):
        # arrange
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            self.userInterface.user_input = ":P"
            # act
            result = self.userInterface.get_command()
            # assert
            self.assertEqual("", fake_stdout.getvalue())
            self.assertEqual(result, Command.UNKNOWN)

    commands = [["help", Command.HELP], ["support", Command.SUPPORT], ["credits", Command.CREDITS],
                ["quit", Command.QUIT], ["move", Command.MOVE], ["drink", Command.DRINK], ["grab", Command.GRAB],
                ["inventory", Command.INVENTORY], ["drop", Command.DROP], ["light", Command.LIGHT],
                ["extinguish", Command.EXTINGUISH]]

    def test_get_command(self):
        for data in self.commands:
            with self.subTest():
                # arrange
                with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
                    self.userInterface.user_input = data[0]
                    # act
                    result = self.userInterface.get_command()
                    # assert
                    self.assertEqual(result, data[1])

    def test_support(self):
        self.userInterface.resource_manager.strings = {"support": "SUP!"}
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.support()
            # assert
            self.assertEqual(
                "SUP!\n", fake_stdout.getvalue())

    def test_help_function(self):
        # arrange
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            commands = {"UNKNOWN": "", "HELP": "HELP", "SUPPORT": "SUPPORT", "CREDITS": "CREDITS", "QUIT": "QUIT",
                        "MOVE": "MOVE", "DRINK": "DRINK", "GRAB": "GRAB", "INVENTORY": "INVENTORY", "DROP": "DROP",
                        "LIGHT": "LIGHT", "EXTINGUISH": "EXTINGUISH", "LOOK": "LOOK", "SAY": "SAY", "REFILL": "REFILL"}

            self.userInterface.resource_manager.strings = {"command": commands}
            # act
            self.userInterface.help_function()
            # assert
            self.assertEqual(
                "\n\nHELP\n"
                "SUPPORT\n"
                "CREDITS\n"
                "QUIT\n"
                "MOVE\n"
                "DRINK\n"
                "GRAB\n"
                "INVENTORY\n"
                "DROP\n"
                "LIGHT\n"
                "EXTINGUISH\n"
                "LOOK\n"
                "SAY\n"
                "REFILL\n\n", fake_stdout.getvalue())

    def test_credits_function(self):
        # arrange
        self.userInterface.resource_manager.strings = {"credits": "creds\n{0}"}
        self.userInterface.resource_manager.supporters = ""
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.credits_function()
            # assert
            self.assertEqual(
                "creds\n\n", fake_stdout.getvalue())

    def test_quit_game(self):
        # arrange
        self.mock_resources.strings = \
            {
                "quit":
                    '''
                    {0}
                    Thank you for playing! Goodbye!
                    ''',
            }
        self.userInterface.resource_manager = self.mock_resources
        self.tracker.print_score = MagicMock(return_value="Score!")
        has_been_called = [False]

        def listener(character=None):
            has_been_called[0] = True

        pub.subscribe(listener, 'quit')

        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.quit_game()
            # assert
            self.assertEqual(
                '''
                    Score!
                    Thank you for playing! Goodbye!
                    
''', fake_stdout.getvalue())
            self.assertTrue(has_been_called[0])

    def test_get_arguments(self):
        # arrange
        self.userInterface.user_input = 'command argument'
        # act
        result = self.userInterface.get_arguments()
        # assert
        self.assertEqual('argument', result[0])

    def test_get_arguments_no_args(self):
        # arrange
        self.userInterface.user_input = 'command'
        # act
        result = self.userInterface.get_arguments()
        # assert
        self.assertEqual(0, len(result))

    def test_resource_path(self):
        # arrange
        file_name = "test"
        # act
        result = self.userInterface.resource_path(file_name)
        # assert
        self.assertEqual(os.path.join(os.path.abspath("."), file_name), result)

    def test_unknown_function(self):
        # arrange
        self.userInterface.resource_manager.strings = {"unknown": "bad command"}
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            self.userInterface.unknown_function()
            # assert
            self.assertEqual("bad command\n", fake_stdout.getvalue())

    def test_drop_empty(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drop": {"empty": "Nothing to drop!\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drop", state="empty")
            # assert
            self.assertEqual("Nothing to drop!\n\n", fake_stdout.getvalue())

    def test_drop_undefined(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drop": {"undefined": "Drop which?\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drop", state="undefined")
            # assert
            self.assertEqual("Drop which?\n\n", fake_stdout.getvalue())

    def test_drop_no_item(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drop": {"no_item": "No item in position \"{0}\"\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drop", state="no_item", items="2")
            # assert
            self.assertEqual("No item in position \"2\"\n\n", fake_stdout.getvalue())

    def test_drop_not_item(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drop": {"no_item": "No item in position \"{0}\"\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drop", state="no_item")
            # assert
            self.assertEqual("No item in position \"\"\n\n", fake_stdout.getvalue())

    def test_drop_dropped(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drop": {"dropped": "Dropped:\n{0}"}},
             "Candle":
                 {
                     0: "A lit candle.",
                     1: "An unlit, unused candle.",
                     2: "An unlit, partially burnt candle.",
                     3: "A burnt nub."
                 },
             }
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drop", state="dropped", items=[Candle()])
            # assert
            self.assertEqual("Dropped:\n"
                             "An unlit, unused candle.\n\n", fake_stdout.getvalue())

    def test_drop_dropped_several(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drop": {"dropped": "Dropped:\n{0}"}},
             "Candle":
                 {
                     0: "A lit candle.",
                     1: "An unlit, unused candle.",
                     2: "An unlit, partially burnt candle.",
                     3: "A burnt nub."
                 },
             }
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drop", state="dropped", items=[Candle(), Candle()])
            # assert
            self.assertEqual("Dropped:\n"
                             "An unlit, unused candle.\nAn unlit, unused candle.\n\n", fake_stdout.getvalue())

    def test_drink_nothing_to_drink(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drink": {"nothing_to_drink": "Nothing here to drink!\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drink", state="nothing_to_drink")
            # assert
            self.assertEqual("Nothing here to drink!\n\n", fake_stdout.getvalue())

    def test_drink_not_thirsty(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drink": {"not_thirsty": "You are not thirsty!\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drink", state="not_thirsty")
            # assert
            self.assertEqual("You are not thirsty!\n\n", fake_stdout.getvalue())

    def test_drink_success(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"drink": {"success": "You feel refreshed!\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.drink", state="success")
            # assert
            self.assertEqual("You feel refreshed!\n\n", fake_stdout.getvalue())

    def test_grab_empty(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"grab": {"empty": "Nothing to grab!\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.grab", state="empty")
            # assert
            self.assertEqual("Nothing to grab!\n\n", fake_stdout.getvalue())

    def test_grab_undefined(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"grab": {"undefined": "Grab which?\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.grab", state="undefined")
            # assert
            self.assertEqual("Grab which?\n\n", fake_stdout.getvalue())

    def test_grab_no_item(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"grab": {"no_item": "No item in position \"{0}\"\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.grab", state="no_item", item="2")
            # assert
            self.assertEqual("No item in position \"2\"\n\n", fake_stdout.getvalue())

    def test_grab_not_item(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"grab": {"no_item": "No item in position \"{0}\"\n"}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.grab", state="no_item")
            # assert
            self.assertEqual("No item in position \"\"\n\n", fake_stdout.getvalue())

    def test_grabbed(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"grab": {"grabbed": "Grabbed:\n{0}"}},
             "Vial":
                 {
                     0: "A vial full of water.",
                     1: "An empty vial."
                 }}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.grab", state="grabbed", item=[Vial()])
            # assert
            self.assertEqual("Grabbed:\nA vial full of water.\n\n", fake_stdout.getvalue())

    def test_grabbed_many(self):
        # arrange
        self.mock_resources.strings = \
            {"Character": {"grab": {"grabbed": "Grabbed:\n{0}"}},
             "Vial":
                 {
                     0: "A vial full of water.",
                     1: "An empty vial."
                 }}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.grab", state="grabbed", item=[Vial(), Vial()])
            # assert
            self.assertEqual("Grabbed:\nA vial full of water.\nA vial full of water.\n\n", fake_stdout.getvalue())

    def test_light_success(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"light":
                    {
                        "success": "Candle lit.\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.light", state="success")
            # assert
            self.assertEqual("Candle lit.\n\n", fake_stdout.getvalue())

    def test_light_undefined(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"light":
                    {
                        "undefined": "Light which candle?\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.light", state="undefined")
            # assert
            self.assertEqual("Light which candle?\n\n", fake_stdout.getvalue())

    def test_light_no_candle(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"light":
                    {
                        "no_candle": "You have no candles to light.\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.light", state="no_candle")
            # assert
            self.assertEqual("You have no candles to light.\n\n", fake_stdout.getvalue())

    def test_light_bad_input(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"light":
                    {
                        "bad_input": "\"{0}\" is not a number.\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.light", state="bad_input")
            # assert
            self.assertEqual("\"\" is not a number.\n\n", fake_stdout.getvalue())

    def test_light_no_item(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"light":
                    {
                        "no_item": "\"{0}\" is not a position in your inventory.\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.light", state="no_item", position="2")
            # assert
            self.assertEqual("\"2\" is not a position in your inventory.\n\n", fake_stdout.getvalue())

    def test_light_not_candle(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"light":
                    {
                        "not_candle": '"{0}" is {1} It cannot be lit.\n',
                    }},
                "Vial":
                    {
                        0: "A vial full of water.",
                        1: "An empty vial."
                    }}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.light", state="not_candle", position="1", item=Vial())
            # assert
            self.assertEqual("\"1\" is A vial full of water. It cannot be lit.\n\n", fake_stdout.getvalue())

    def test_light_success_position(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"light":
                    {
                        "success_position": 'Candle "{0}" lit.\n',
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.light", state="success_position", position="2")
            # assert
            self.assertEqual("Candle \"2\" lit.\n\n", fake_stdout.getvalue())

    def test_extinguish_success(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"extinguish":
                    {
                        "success": "You blow all of your lit candles out.\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.extinguish", state="success")
            # assert
            self.assertEqual("You blow all of your lit candles out.\n\n", fake_stdout.getvalue())

    def test_extinguish_failure(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"extinguish":
                    {
                        "failure": "oops\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.extinguish", state="failure")
            # assert
            self.assertEqual("oops\n\n", fake_stdout.getvalue())

    def test_inventory(self):
        # arrange
        self.mock_resources.strings = {"Character": {"inventory": "Inventory:\n{0}"},
                                       "Candle":
                                           {
                                               0: "A lit candle.",
                                               1: "An unlit, unused candle.",
                                           }}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.inventory", items=[Candle(), Candle()])
            # assert
            self.assertEqual("Inventory:\n1: An unlit, unused candle.\n2: An unlit, unused candle.\n\n",
                             fake_stdout.getvalue())

    def test_move_bad_input(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"move":
                    {
                        "bad_input": "I'm sorry, I don't know that direction. Please use:\n{0}",

                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.move", state="bad_input", direction="Back\n")
            # assert
            self.assertEqual("I'm sorry, I don't know that direction. Please use:\nback\n\n", fake_stdout.getvalue())

    def test_move_no_exit(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"move":
                    {
                        "no_exit": "There is no exit that direction.\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.move", state="no_exit")
            # assert
            self.assertEqual("There is no exit that direction.\n\n", fake_stdout.getvalue())

    def test_move_difficult(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"move":
                    {
                        "difficult":
                            {0: "You clamber over the debris in your way.\n"},
                        "travel": "You go {0}.\n"

                    }}}
        self.userInterface.resource_manager = self.mock_resources
        room = MagicMock()
        room.is_lit = MagicMock(return_value=True)
        room.is_difficult = True
        room.difficult_type = 0
        room.exits = list()
        room.items = list()
        player = MagicMock()
        player.current_room = room
        self.userInterface.player = player
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.move", state="difficult")
            # assert
            self.assertEqual("You clamber over the debris in your way.\n\n", fake_stdout.getvalue())

    def test_move_exhausted(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"move":
                    {
                        "exhausted": "You die of thirst while trying to climb.\n",
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.move", state="exhausted")
            # assert
            self.assertEqual("You die of thirst while trying to climb.\n\n", fake_stdout.getvalue())

    def test_move_travel(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"move":
                    {
                        "travel": "You go {0}.\n",

                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.move", state="travel", direction="Right")
            # assert
            self.assertEqual("You go right.\n\n", fake_stdout.getvalue())

    def test_character_description_thirst(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"description":
                    {"thirst":
                        {
                            0: "You are feeling pretty good all things considered.\n",
                        }}}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.description.thirst", state=0)
            # assert
            self.assertEqual("You are feeling pretty good all things considered.\n\n", fake_stdout.getvalue())

    def test_character_description_exits(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"description":
                    {
                        "exits": "\nThere {0} door(s)",
                        "number_words":
                            {
                                1: "is one",
                            },
                        "direction_words":
                            {
                                0: "in front of you",
                            }
                    }}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.description.exits", directions=[0])
            # assert
            self.assertEqual("\nThere is one door(s), in front of you.\n", fake_stdout.getvalue())

    def test_character_description(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"description":
                    {
                        "direction_words":
                            {
                                0: "in front of you",
                                1: "to your right",
                                2: "behind you",
                                3: "to your left"
                            },
                    },
                    "distant_description": "You hear softly echoing footsteps"}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.distant_description", direction=1)
            # assert
            self.assertEqual("You hear softly echoing footsteps to your right\n", fake_stdout.getvalue())

    def test_monster_kill(self):
        # arrange
        self.mock_resources.strings = \
            {
                "Monster":
                    {"kill": '\nYou feel teeth sink into your neck and shoulder and claws slicing into your side.\n'}
            }
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Monster.kill")
            # assert
            self.assertEqual("\nYou feel teeth sink into your neck and shoulder and claws slicing into your side.\n\n",
                             fake_stdout.getvalue())

    def test_monster_distant_description(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"description":
                    {
                        "direction_words":
                            {
                                0: "in front of you",
                                1: "to your right",
                                2: "behind you",
                                3: "to your left"
                            }}},
                "Monster": {"distant_description": "You hear echoing wheezing"}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Monster.distant_description", direction=3)
            # assert
            self.assertEqual("You hear echoing wheezing to your left\n",
                             fake_stdout.getvalue())

    def test_monster_description(self):
        # arrange
        self.mock_resources.strings = \
            {"Character":
                {"description":
                    {
                        "direction_words":
                            {
                                0: "in front of you",
                                1: "to your right",
                                2: "behind you",
                                3: "to your left"
                            }}},
                "Monster": {"description": "You hear ragged breaths"}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Monster.description", direction=2)
            # assert
            self.assertEqual("You hear ragged breaths behind you\n",
                             fake_stdout.getvalue())

    def test_look_bad_input(self):
        self.mock_resources.strings = \
            {"Character":
                {
                    "look":
                        {
                            "bad_input": "I'm sorry, I don't know that direction. Please use:\n{0}",
                        }
                }}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.look", state="bad_input", direction="Back\n")
            # assert
            self.assertEqual("I'm sorry, I don't know that direction. Please use:\nback\n\n",
                             fake_stdout.getvalue())

    def test_look_bad_input_garbage_direction(self):
        self.mock_resources.strings = \
            {"Character":
                {
                    "look":
                        {
                            "bad_input": "I'm sorry, I don't know that direction. Please use:\n{0}",
                        }
                }}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.look", state="bad_input")
            # assert
            self.assertEqual("I'm sorry, I don't know that direction. Please use:\n\n",
                             fake_stdout.getvalue())

    def test_look_no_exit(self):
        self.mock_resources.strings = \
            {"Character":
                {
                    "look":
                        {
                            "no_exit": "There is no exit that direction.\n",
                        }
                }}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.look", state="no_exit")
            # assert
            self.assertEqual("There is no exit that direction.\n\n",
                             fake_stdout.getvalue())

    def test_look_success(self):
        self.mock_resources.strings = \
            {"Character":
                {
                    "look":
                        {
                            "success": "You look {0}:"
                        },
                    "description":
                        {

                            "direction_words":
                                {
                                    0: "in front of you",
                                    1: "to your right",
                                    2: "behind you",
                                    3: "to your left"
                                },
                        }
                }}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Character.look", state="success", direction=2)
            # assert
            self.assertEqual("You look behind you:\n",
                             fake_stdout.getvalue())

    def test_say(self):
        self.mock_resources.strings = \
            {"Character": {"say": "You say: {0}"}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("say", said="hello, how are you?", character=self.userInterface.player)
            # assert
            self.assertEqual("You say: hello, how are you?\n", fake_stdout.getvalue())

    def test_teleport_engraving(self):
        self.mock_resources.strings = \
            {"Teleport": {"engraving": "You see engraved on the ground: \"{0}\""}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Teleport.engraving", word="sup?")
            # assert
            self.assertEqual("You see engraved on the ground: \"sup?\"\n", fake_stdout.getvalue())

    def test_teleport_teleport(self):
        self.mock_resources.strings = \
            {"Teleport": {"teleport": "BAMF"}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Teleport.teleport")
            # assert
            self.assertEqual("BAMF\n", fake_stdout.getvalue())

    def test_end_description(self):
        self.mock_resources.strings = \
            {"End": {"description": "the end"}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("End.description")
            # assert
            self.assertEqual("the end\n", fake_stdout.getvalue())

    def test_end_escape(self):
        self.mock_resources.strings = \
            {"End": {"escape": "yeet"}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("End.escape")
            # assert
            self.assertEqual("yeet\n", fake_stdout.getvalue())

    def test_fountain_description(self):
        self.mock_resources.strings = \
            {"Fountain": {"description": "water..."}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Fountain.description")
            # assert
            self.assertEqual("water...\n", fake_stdout.getvalue())

    def test_fountain_refill(self):
        self.mock_resources.strings = \
            {"Fountain": {"refill": "water..."}}
        self.userInterface.resource_manager = self.mock_resources
        with patch('sys.stdout', new=io.StringIO()) as fake_stdout:
            # act
            pub.sendMessage("Fountain.refill")
            # assert
            self.assertEqual("water...\n", fake_stdout.getvalue())


if __name__ == '__main__':
    unittest.main()
