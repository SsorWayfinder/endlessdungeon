import unittest
from endlessdungeon import Command


class MyTestCase(unittest.TestCase):

    def test_index(self):
        index = -1
        for command in Command.Command:
            with self.subTest():
                index += 1
                self.assertEqual(index, command.index, command.name
                                 + " index incorrect, should be " + str(index)
                                 + "but is " + str(command.index))
                self.assertEqual(index > 4, command.character_command)


if __name__ == '__main__':
    unittest.main()
