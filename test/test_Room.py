import unittest
import unittest.mock as mock

from pubsub import pub

from endlessdungeon import Room
from endlessdungeon.items import Vial, Candle
from endlessdungeon.CardinalDirection import CardinalDirection
from endlessdungeon.characters.Character import Character


class TestRoom(unittest.TestCase):

    def setUp(self):
        Room.Room.room_grid = dict()
        self.room = Room.Room(1, 1)

    def test_init(self):
        # assert
        self.assertEqual(self.room.is_difficult, False)
        self.assertEqual(0, self.room.difficult_type)
        self.assertEqual(self.room.has_been_generated, False)
        assert isinstance(self.room.exits, list)
        self.assertEqual(len(self.room.exits), 0)
        assert isinstance(self.room.items, list)
        self.assertEqual(len(self.room.items), 0)
        self.assertFalse(self.room.has_been_generated)
        self.assertEqual(1, self.room.x)
        self.assertEqual(1, self.room.y)
        self.assertTrue(Room.Room.room_grid[1][1], self.room)

    def test_generate_has_been_generated(self):
        # act
        self.room.generate()
        result = self.room.has_been_generated
        # assert
        self.assertTrue(result)

    def test_Room_exits(self):
        # arrange
        self.setUp()
        mock_random = unittest.mock.Mock(side_effect=[0] * 1 + [1] * 7)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
        result = self.room.exits
        # assert
        self.assertEqual(len(result), 3)
        for new_room in result:
            assert new_room.exits[0] == self.room
        mock_random.assert_has_calls([unittest.mock.call(0, 3)])

    def test_Room_exits_existing_not_generated(self):
        # arrange
        self.setUp()
        self.room.exits = list()
        Room.Room(0, 1)
        Room.Room(2, 1)
        Room.Room(1, 0)
        Room.Room(1, 2)

        mock_random = unittest.mock.Mock(side_effect=[0] * 4 + [1] * 7)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
        result = self.room.exits
        # assert
        self.assertEqual(len(result), 4)
        for new_room in result:
            assert new_room.exits[0] == self.room
        mock_random.assert_has_calls(
            [unittest.mock.call(0, 0), unittest.mock.call(0, 1), unittest.mock.call(0, 2), unittest.mock.call(0, 3)])

    exit_positions = [[0, 1], [1, 2], [2, 1], [1, 0]]

    def test_minimum_Room_exit_positions(self):
        # arrange
        mock_random = mock.Mock(return_value=0)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
            result = self.room.exits
            # assert
            for positions, exit_room in zip(self.exit_positions, result):
                self.assertEqual(positions[0], exit_room.x)
                self.assertEqual(positions[1], exit_room.y)

    def test_minimum_Room_exits_added_to_ungenerated(self):
        # arrange
        for position in self.exit_positions:
            neighbor = Room.Room(position[0], position[1])
        mock_random = mock.Mock(return_value=0)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
            result = self.room.exits
            # assert
            self.assertEqual(4, len(result))

    def test_minimum_Room_No_Inconsistent_Exit(self):
        # arrange
        for position in self.exit_positions:
            with self.subTest():
                self.setUp()
                neighbor = Room.Room(position[0], position[1])
                neighbor.has_been_generated = True
                # act
                self.room.generate()
                # assert
                self.assertFalse(any(exit_room for exit_room in self.room.exits
                                     if exit_room.x == position[0]
                                     and exit_room.y == position[1]),
                                 "Improperly added an exit to " + str(position[0]) + ", " + str(position[1]))

    def test_minimum_items(self):
        # arrange
        mock_random = mock.Mock(return_value=1)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
        result = self.room.items
        # assert
        self.assertEqual(len(result), 0)
        mock_random.assert_any_call(0, 19)

    def test_has_vial(self):
        # arrange
        values = [1, 1, 1, 1, 1, 0, 1, 1]

        def side_effect(a, b):
            return values.pop()

        mock_random = mock.Mock(side_effect=side_effect)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
        result = self.room.items
        # assert
        self.assertEqual(len(result), 1)
        mock_random.assert_any_call(0, 79)
        self.assertIsInstance(result[0], Vial.Vial)

    def test_has_candle(self):
        # arrange
        values = [1, 1, 1, 1, 0, 1, 1, 1]

        def side_effect(a, b):
            return values.pop()

        mock_random = mock.Mock(side_effect=side_effect)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
        result = self.room.items
        # assert
        self.assertEqual(len(result), 1)
        mock_random.assert_has_calls([mock.call(0, 79), mock.call(0, 19)])
        self.assertIsInstance(result[0], Candle.Candle)

    def test_is_not_difficult(self):
        # arrange
        mock_random = mock.Mock(return_value=4)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
        result = self.room.is_difficult
        # assert
        self.assertFalse(result)
        mock_random.assert_any_call(0, 9)

    def test_is_difficult(self):
        # arrange
        mock_random = mock.Mock(return_value=0)
        with mock.patch('random.randint', mock_random):
            # act
            self.room.generate()
        result = self.room.is_difficult
        # assert
        self.assertTrue(result)
        mock_random.assert_any_call(0, 9)
        mock_random.assert_any_call(0, 4)

    def test_first_room_is_lit(self):
        # arrange
        self.room = Room.Room(0, 0)
        # assert
        self.assertTrue(self.room.is_lit())

    def test_forward_first_room_dark_is_not_lit(self):
        # arrange
        self.room = Room.Room(0, 1)
        # assert
        self.assertEqual(False, self.room.is_lit())

    def test_description_right_first_room_dark(self):
        # arrange
        self.room = Room.Room(1, 0)
        # assert
        self.assertEqual(False, self.room.is_lit())

    def test_is_lit_by_item_in_room(self):
        # arrange
        candle = Candle.Candle()
        candle.is_lit = True
        self.room.items.append(candle)
        character = Character()
        character.inventory = list()
        self.room.add_character(character)
        # assert
        self.assertTrue(self.room.is_lit())

    def test_is_lit_by_item_in_character_inventory(self):
        # arrange
        candle = Candle.Candle()
        candle.is_lit = True
        character = Character()
        character.inventory.append(candle)
        character.inventory.append(Candle.Candle())
        self.room.add_character(character)
        # assert
        self.assertTrue(self.room.is_lit())
        self.assertFalse(self.room.items.__contains__(candle))

    coors_direction = [[1, 2, CardinalDirection.PY],
                       [2, 1, CardinalDirection.PX],
                       [1, 0, CardinalDirection.NY],
                       [0, 1, CardinalDirection.NX]]

    def test_get_exit_by_direction(self):
        # arrange
        for data in self.coors_direction:
            with self.subTest():
                new_room = Room.Room(data[0], data[1])
                self.room.exits.append(new_room)
                # act
                result = self.room.get_exit(data[2])
                # assert
                self.assertEqual(new_room, result)

    def test_get_exit_attempt_to_get_None_room(self):
        # arrange
        self.room.exits.append(None)
        # act
        result = self.room.get_exit(CardinalDirection.PX)
        # assert
        self.assertIsNone(result)

    def test_explored_event(self):
        # arrange
        expected_character = Character()

        has_been_called = [False]
        called_character = list()

        def listener(character):
            called_character.append(character)
            has_been_called[0] = True

        pub.subscribe(listener, 'explored')
        # act
        self.room.add_character(expected_character)
        # assert
        self.assertEqual(called_character[0], expected_character)
        self.assertTrue(has_been_called[0])
        self.assertEqual(expected_character, self.room.explored[0])

    def test_characters(self):
        # arrange
        character = Character()
        # act
        self.room.add_character(character)
        # assert
        self.assertEqual(character.current_room, self.room)

    def test_existing_room_returned_if_already_in_grid(self):
        # arrange
        old_room = Room.Room(1, 1)
        # act
        new_room = Room.create(1, 1)
        # assert
        self.assertEqual(new_room, old_room)


if __name__ == '__main__':
    unittest.main()
