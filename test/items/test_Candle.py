import unittest
from pubsub import pub

from endlessdungeon.items.Candle import Candle


class TestCandle(unittest.TestCase):

    def setUp(self):
        self.candle = Candle()

    def test_init(self):
        self.assertEqual(False, self.candle._is_lit)
        self.assertEqual(30, self.candle.time_left)
        self.assertEqual(1, self.candle.state())

    def test_action_event_reduces_time_left(self):
        # arrange
        self.candle.is_lit = True
        self.candle.time_left = 2
        # act
        pub.sendMessage("action")
        # assert
        self.assertEqual(1, self.candle.time_left)
        self.assertTrue(self.candle.is_lit)
        self.assertEqual(0, self.candle.state())

    def test_action_event_puts_out(self):
        # arrange
        self.candle.is_lit = True
        self.candle.time_left = 1
        # act
        pub.sendMessage("action")
        # assert
        self.assertEqual(0, self.candle.time_left)
        self.assertEqual(False, self.candle.is_lit)
        self.assertEqual(3, self.candle.state())

    def test_cannot_light_no_time_left(self):
        # arrange
        self.candle.time_left = 0
        # act
        self.candle.is_lit = True
        # assert
        self.assertEqual(False, self.candle.is_lit)
        self.assertEqual(3, self.candle.state())

    def test_cannot_just_enough_time_left(self):
        # arrange
        self.candle.time_left = 1
        # act
        self.candle.is_lit = True
        # assert
        self.assertTrue(self.candle.is_lit)
        self.assertEqual(0, self.candle.state())

    def test_unlit_just_enough_time_left(self):
        # arrange
        self.candle.time_left = 1
        # act
        # assert
        self.assertEqual(2, self.candle.state())

    def test_unlit_unburnt(self):
        # arrange
        self.candle.time_left = 30
        self.candle.is_lit = False
        # act
        # assert
        self.assertEqual(1, self.candle.state())

    def test_unlit_lightly_burnt(self):
        # arrange
        self.candle.time_left = 29
        self.candle.is_lit = False
        # act
        # assert
        self.assertEqual(2, self.candle.state())
