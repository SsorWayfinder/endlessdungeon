import unittest
from endlessdungeon.items.Vial import Vial


class TestVial(unittest.TestCase):

    def setUp(self):
        self.vial = Vial()

    def test_Vial(self):
        self.assertTrue(self.vial.is_full)

    def test_consume(self):
        # act
        self.assertTrue(self.vial.consume())
        # assert
        self.assertEqual(self.vial.is_full, False)

    def test_consume_empty(self):
        # arrange
        self.vial.is_full = False
        # act/assert
        self.assertFalse(self.vial.consume())


if __name__ == '__main__':
    unittest.main()
