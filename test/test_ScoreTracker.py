import unittest
import pubsub

from endlessdungeon.ScoreTracker import ScoreTracker
from endlessdungeon.characters.Character import Character
from endlessdungeon.characters.Monster import Monster


class TestScoreTracker(unittest.TestCase):

    def setUp(self):
        self.character = Character()
        self.tracker = ScoreTracker(self.character)

    def test_ScoreTracker(self):
        self.assertEqual(0, self.tracker.moves)
        self.assertEqual(0, self.tracker.rooms_explored)
        self.assertEqual(0, self.tracker.vials_drank)
        self.assertEqual(0, self.tracker.times_exerted)
        self.assertEqual(0, self.tracker.score)
        self.assertEqual("Score:\n"
                         "Escaped:              False x  100000 =      0\n"
                         "Rooms Explored:           0 x    1000 =      0\n"
                         "Monsters Slain:           0 x    1000 =      0\n"
                         "Vials Drank:              0 x     100 =      0\n"
                         "Piles of Rubble Climbed:  0 x      50 =      0\n"
                         "Moves Made:               0 x       1 =      0\n"
                         "______________________________________________\n"
                         "Total:                                       0\n",
                         self.tracker.print_score())

    startEnd = [[0, 1], [1, 2], [100, 101]]

    def test_moves(self):
        for data in self.startEnd:
            with self.subTest():
                # arrange
                self.tracker.moves = data[0]
                # act
                pubsub.pub.sendMessage('moved', character=self.character)
                # assert
                self.assertEqual(data[1], self.tracker.moves)

    def test_rooms_explored(self):
        for data in self.startEnd:
            with self.subTest():
                # arrange
                self.tracker.rooms_explored = data[0]
                # act
                pubsub.pub.sendMessage('explored', character=self.character)
                # assert
                self.assertEqual(data[1], self.tracker.rooms_explored)

    def test_vials_drank(self):
        for data in self.startEnd:
            with self.subTest():
                # arrange
                self.tracker.vials_drank = data[0]
                # act
                pubsub.pub.sendMessage('drank', character=self.character)
                # assert
                self.assertEqual(data[1], self.tracker.vials_drank)

    def test_times_exerted(self):
        for data in self.startEnd:
            with self.subTest():
                # arrange
                self.tracker.times_exerted = data[0]
                # act
                pubsub.pub.sendMessage('exerted', character=self.character)
                # assert
                self.assertEqual(data[1], self.tracker.times_exerted)

    def test_monsters_slain(self):
        for data in self.startEnd:
            with self.subTest():
                # arrange
                self.tracker.monsters_slain = data[0]
                # act
                pubsub.pub.sendMessage('died', character=Monster(0, 0))
                # assert
                self.assertEqual(data[1], self.tracker.monsters_slain)

    def test_escaped(self):
        # act
        pubsub.pub.sendMessage("End.escape")
        # assert
        self.assertTrue(self.tracker.escaped)

    def test_move_score(self):
        # act
        self.tracker.moves = 1
        # assert
        self.assertEqual(1, self.tracker.score)

    def test_rooms_explored_score(self):
        # act
        self.tracker.rooms_explored = 1
        # assert
        self.assertEqual(1000, self.tracker.score)

    def test_vials_drank_score(self):
        # act
        self.tracker.vials_drank = 1
        # assert
        self.assertEqual(100, self.tracker.score)

    def test_times_exerted_score(self):
        # act
        self.tracker.times_exerted = 1
        # assert
        self.assertEqual(50, self.tracker.score)

    def test_monsters_slain_score(self):
        # act
        self.tracker.monsters_slain = 1
        # assert
        self.assertEqual(1000, self.tracker.score)

    def test_escaped_scope(self):
        # act
        self.tracker.escaped = True
        # assert
        self.assertEqual(100000, self.tracker.score)


if __name__ == '__main__':
    unittest.main()
