import unittest
import unittest.mock as mock

from pubsub import pub

from endlessdungeon.fixtures import Teleport
from endlessdungeon import Room


class TestTeleport(unittest.TestCase):

    def tearDown(self):
        Room.Room.room_grid = dict()

    def test_init(self):
        # arrange
        mock_random_int = unittest.mock.Mock(return_value=5)
        mock_random_choices = unittest.mock.Mock(return_value='aaaaa')
        with mock.patch('random.randint', mock_random_int):
            with mock.patch('random.choices', mock_random_choices):
                # act
                teleport = Teleport.Teleport()
                # assert
                self.assertEqual('aaaaa', teleport.text)
                self.assertEqual(5, teleport.room.x)
                self.assertEqual(5, teleport.room.y)
                mock_random_int.assert_has_calls([unittest.mock.call(-10, 10), unittest.mock.call(-10, 10),
                                                  unittest.mock.call(5, 10)])

    def test_say_event(self):
        # arrange
        has_been_called1 = [False]

        def listener1():
            has_been_called1[0] = True

        pub.subscribe(listener1, "Teleport.teleport")

        has_been_called2 = [False]
        rooms = list()

        def listener2(room):
            has_been_called2[0] = True
            rooms.append(room)

        pub.subscribe(listener2, "Character.Room.description")

        teleport = Teleport.Teleport()
        teleport.text = "test"
        mock_character = unittest.mock.Mock()
        mock_character.current_room.x = 0
        mock_character.current_room.y = 0
        mock_room = unittest.mock.Mock()
        mock_new_room = unittest.mock.MagicMock()
        mock_room.create = unittest.mock.MagicMock(return_value=mock_new_room)
        mock_random = unittest.mock.Mock(return_value=1)
        with mock.patch('endlessdungeon.fixtures.Teleport.Room', mock_room):
            with mock.patch('random.randint', mock_random):
                # act
                pub.sendMessage("say", said=["test"], character=mock_character)
                # assert
                mock_room.create.assert_called_with(1, 1)
                self.assertEqual(mock_new_room, mock_character.current_room)
                mock_character.exert.assert_called()
                mock_new_room.generate.assert_called()
                mock_random.assert_has_calls([unittest.mock.call(-100, 100), unittest.mock.call(-100, 100)])
                self.assertTrue(has_been_called1[0])
                self.assertTrue(has_been_called2[0])
                self.assertEqual(mock_new_room, rooms[0])

    def test_description(self):
        teleport = Teleport.Teleport()
        mock_room = unittest.mock.MagicMock()
        teleport.room = mock_room
        teleport.text = "test"
        has_been_called = [False]
        words = list()

        def listener(word):
            has_been_called[0] = True
            words.append(word)

        pub.subscribe(listener, 'Teleport.engraving')
        # act
        pub.sendMessage("Character.Room.description", room=teleport.room)
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("test", words[0])
