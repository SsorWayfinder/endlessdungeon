import unittest
from unittest import mock
from pubsub import pub

from endlessdungeon.fixtures.Fountain import Fountain
from endlessdungeon import Room
from endlessdungeon.items.Vial import Vial


class TestFountain(unittest.TestCase):

    def setUp(self):
        self.fountain = Fountain(0, 0)

    def tearDown(self):
        Room.Room.room_grid = dict()

    def test_description(self):
        # arrange
        mock_room = unittest.mock.MagicMock()
        self.fountain.room = mock_room

        has_been_called = [False]

        def listener():
            has_been_called[0] = True

        pub.subscribe(listener, 'Fountain.description')
        # act
        pub.sendMessage("Character.Room.description", room=mock_room)
        # assert
        self.assertTrue(has_been_called[0])

    def test_refill(self):
        # arrange
        mock_room = unittest.mock.MagicMock()
        self.fountain.room = mock_room

        mock_vial = mock.Mock(is_full=False)
        mock_vial.__class__ = Vial
        mock_character = mock.Mock(current_room=mock_room, inventory=[mock_vial])
        has_been_called = [False]

        def listener():
            has_been_called[0] = True

        pub.subscribe(listener, 'Fountain.refill')

        # act
        pub.sendMessage("refill", character=mock_character)
        # assert
        self.assertTrue(mock_vial.is_full)
        self.assertTrue(has_been_called[0])




