import unittest
from unittest import mock
from pubsub import pub

from endlessdungeon.characters.Monster import Monster
from endlessdungeon.characters.Character import Character
from endlessdungeon.CardinalDirection import CardinalDirection
from endlessdungeon.items.Vial import Vial
from endlessdungeon.items.Candle import Candle
from endlessdungeon.Room import Room
from endlessdungeon.RelativeDirection import RelativeDirection


class TestMonster(unittest.TestCase):

    def setUp(self):
        self.monster = Monster(0, 0)
        self.mock_room = mock.MagicMock()
        self.mock_room.items = list()
        self.monster.current_room = self.mock_room
        self.mock_room.get_exit = mock.MagicMock(return_value=None)
        self.mock_room.is_difficult = False

    def tearDown(self):
        Room.room_grid = dict()

    def test_init(self):
        # arrange
        mock_random = mock.Mock(return_value=1)
        with mock.patch('random.randint', mock_random):
            # act
            monster = Monster(0, 0)
            # assert
            self.assertEqual(1, monster.current_room.x)
            self.assertEqual(1, monster.current_room.y)
            mock_random.assert_any_call(-2, 2)

    def test_init_spawn_too_close(self):
        # arrange
        values = [1, 1, 1, 1, 1, 1, 1, 2, 0, 1, 0]

        def side_effect(a, b):
            return values.pop()

        mock_random = mock.Mock(side_effect=side_effect)
        with mock.patch('random.randint', mock_random):
            # act
            monster = Monster(0, 0)
            # assert
            self.assertEqual(1, monster.current_room.x)
            self.assertEqual(2, monster.current_room.y)
            calls = [mock.call(-2, 2), mock.call(-2, 2), mock.call(-2, 2), mock.call(-2, 2)]
            mock_random.assert_has_calls(calls)

    def test_change_room_if_no_exit(self):
        Room.room_grid = dict()
        values = [-2, -1, 2, 2]
        bad_room = Room(2, 2)
        bad_room.has_been_generated = True

        good_room = Room(-1, -2)
        good_room.exits.append(Room(-1, -1))
        good_room.has_been_generated = True

        def side_effect(a, b):
            return values.pop()

        mock_random = mock.Mock(side_effect=side_effect)
        with mock.patch('random.randint', mock_random):
            # act
            monster = Monster(0, 0)
            # assert
            self.assertEqual(-1, monster.current_room.x)
            self.assertEqual(-2, monster.current_room.y)

    start_orientation_direction_description_end_orientation = [
        [CardinalDirection.PY, "forward", CardinalDirection.PY],
        [CardinalDirection.PX, "forward", CardinalDirection.PX],
        [CardinalDirection.NY, "forward", CardinalDirection.NY],
        [CardinalDirection.NX, "forward", CardinalDirection.NX],

        [CardinalDirection.PY, "right", CardinalDirection.PX],
        [CardinalDirection.PX, "right", CardinalDirection.NY],
        [CardinalDirection.NY, "right", CardinalDirection.NX],
        [CardinalDirection.NX, "right", CardinalDirection.PY],

        [CardinalDirection.NX, "back", CardinalDirection.PX],
        [CardinalDirection.NY, "back", CardinalDirection.PY],
        [CardinalDirection.PX, "back", CardinalDirection.NX],
        [CardinalDirection.PY, "back", CardinalDirection.NY],

        [CardinalDirection.PY, "left", CardinalDirection.NX],
        [CardinalDirection.PX, "left", CardinalDirection.PY],
        [CardinalDirection.NY, "left", CardinalDirection.PX],
        [CardinalDirection.NX, "left", CardinalDirection.NY],
    ]

    def test_act_move_back_from_difficult_room(self):
        # arrange
        for data in self.start_orientation_direction_description_end_orientation:
                with self.subTest():
                    self.setUp()
                    expected_room = mock.MagicMock()

                    def side_effect(direction):
                        if direction == data[2]:
                            return expected_room

                    mock_get_exit = mock.MagicMock(side_effect=side_effect)
                    self.monster.current_room = mock.MagicMock()
                    self.monster.current_room.get_exit = mock_get_exit
                    mock_remove = mock.MagicMock()
                    self.monster.current_room.remove_character = mock_remove
                    self.monster.cardinal_mapping.rotate(4 - self.monster.cardinal_mapping.index(data[0]))

                    self.monster.current_room.is_difficult = True
                    self.monster.thirst = 2

                    mock_random = mock.Mock(return_value=[[self.monster.move, [data[1]], 1]])
                    with mock.patch('random.choices', mock_random):
                        # act
                        self.monster.act()
                        # assert
                        mock_get_exit.assert_any_call(data[2])
                        mock_remove.assert_called_with(self.monster)
                        self.assertEqual(expected_room, self.monster.current_room)
                        self.assertEqual(data[2], self.monster.orientation)
                        if data[1] is "back":
                            mock_random.assert_called_once_with([[self.monster.move, [data[1].upper()], 5]], weights=[5])
                        else:
                            mock_random.assert_called_once_with([[self.monster.move, [data[1].upper()], 1]],
                                                                weights=[1])

    def test_act_move_toward_player(self):
        # arrange
        for data in self.start_orientation_direction_description_end_orientation:
            with self.subTest():
                self.setUp()
                expected_room = mock.MagicMock()

                def side_effect(direction):
                    if direction == data[2]:
                        return expected_room

                mock_get_exit = mock.MagicMock(side_effect=side_effect)
                self.monster.current_room = mock.MagicMock()
                self.monster.current_room.get_exit = mock_get_exit
                mock_remove = mock.MagicMock()
                self.monster.current_room.remove_character = mock_remove
                self.monster.cardinal_mapping.rotate(4 - self.monster.cardinal_mapping.index(data[0]))

                def another_side_effect():
                    self.monster.heard_player.append(RelativeDirection[data[1].upper()])

                self.monster.description = mock.MagicMock(side_effect=another_side_effect)

                mock_random = mock.Mock(return_value=[[self.monster.move, [data[1]], 11]])
                with mock.patch('random.choices', mock_random):
                    # act
                    self.monster.act()
                    # assert
                    mock_get_exit.assert_any_call(data[2])
                    mock_remove.assert_called_with(self.monster)
                    self.assertEqual(expected_room, self.monster.current_room)
                    self.assertEqual(data[2], self.monster.orientation)
                    mock_random.assert_called_once_with([[self.monster.move, [data[1].upper()], 11]], weights=[11])

    def test_act_drink_from_room(self):
        # arrange
        self.mock_room.items = [Vial()]
        self.monster.thirst = 1
        mock_random = mock.Mock(return_value=[[self.monster.drink, [None], 2]])
        with mock.patch('random.choices', mock_random):
            # act
            self.monster.act()
            # assert
            self.assertEqual(0, self.monster.thirst)
            mock_random.assert_called_once_with(
                [[self.monster.drink, [None], 2], [self.monster.grab, ["1"], 2]], weights=[2, 2])

    def test_act_drink_from_room_very_thirsty(self):
        # arrange
        self.mock_room.items = [Vial()]
        self.monster.thirst = 4
        mock_random = mock.Mock(return_value=[[self.monster.drink, [None], 8]])
        with mock.patch('random.choices', mock_random):
            # act
            self.monster.act()
            # assert
            self.assertEqual(3, self.monster.thirst)
            mock_random.assert_called_once_with(
                [[self.monster.drink, [None], 8], [self.monster.grab, ["1"], 8]], weights=[8, 8])

    def test_act_drink_from_inventory(self):
        # arrange
        self.monster.inventory.append(Vial())
        self.monster.thirst = 1
        # act
        self.monster.act()
        # assert
        self.assertEqual(0, self.monster.thirst)

    def test_kill_character_in_same_room(self):
        # arrange
        has_been_called = [False]

        def listener():
            has_been_called[0] = True

        pub.subscribe(listener, "Monster.kill")

        character = Character()
        room = mock.MagicMock()
        character.current_room = room
        room.get_characters = mock.MagicMock(return_value=[character, self.monster])
        mock_get_exit = mock.MagicMock(return_value=room)
        self.mock_room.get_exit = mock_get_exit
        mock_remove = mock.MagicMock()
        self.monster.current_room.remove_character = mock_remove
        # act
        result = self.monster.act()
        # assert
        self.assertEqual(False, character.is_alive)
        self.assertTrue(has_been_called[0])

    def test_kill_character_in_moving_into_the_same_room(self):
        # arrange
        has_been_called = [False]

        def listener():
            has_been_called[0] = True

        pub.subscribe(listener, "Monster.kill")

        character = Character()
        room = mock.MagicMock()
        character.current_room = room
        room.get_characters = mock.MagicMock(return_value=[character, self.monster])
        self.monster.current_room = room
        # act
        result = self.monster.act()
        # assert
        self.assertEqual(False, character.is_alive)
        self.assertTrue(has_been_called[0])

    def test_pick_from_all_if_dark(self):
        # arrange
        self.mock_room.is_lit = mock.MagicMock(return_value=False)
        self.monster.inventory = list()
        mock_random = mock.Mock(return_value=[[self.monster.grab, ["all"], 3]])
        with mock.patch('random.choices', mock_random):
            # act
            self.monster.act()
            # assert
            mock_random.assert_called_once_with(
                [
                    [self.monster.move, ['FORWARD'], 1],
                    [self.monster.move, ['RIGHT'], 1],
                    [self.monster.move, ['BACK'], 1],
                    [self.monster.move, ['LEFT'], 1],
                    [self.monster.grab, ["all"], 3],
                ],
                weights=[1, 1, 1, 1, 3])

    def test_drink_or_grab_vial_on_floor(self):
        self.mock_room.is_lit = mock.MagicMock(return_value=True)
        self.mock_room.items = [Vial()]
        self.monster.inventory = list()
        mock_random = mock.Mock(return_value=[[self.monster.grab, ["1"], 0]])
        with mock.patch('random.choices', mock_random):
            # act
            self.monster.act()
            # assert
            mock_random.assert_called_once_with(
                [[self.monster.drink, [None], 0], [self.monster.grab, ["1"], 0]], weights=[0, 0])

    def test_grab_candle_on_floor(self):
        self.mock_room.is_lit = mock.MagicMock(return_value=True)
        self.mock_room.items = [Candle()]
        self.monster.inventory = list()
        mock_random = mock.Mock(return_value=[[self.monster.grab, ["1"], 0]])
        with mock.patch('random.choices', mock_random):
            # act
            self.monster.act()
            # assert
            mock_random.assert_called_once_with([[self.monster.grab, ["1"], 0]], weights=[0])

    def test_light_candle_in_inventory(self):
        self.mock_room.is_lit = mock.MagicMock(return_value=False)
        self.monster.inventory = list()
        self.monster.inventory.append(Candle())
        mock_random = mock.Mock(return_value=[[self.monster.light, ["1"], 0]])
        with mock.patch('random.choices', mock_random):
            # act
            self.monster.act()
            # assert
            mock_random.assert_called_once_with(
                [
                    [self.monster.move, ['FORWARD'], 1],
                    [self.monster.move, ['RIGHT'], 1],
                    [self.monster.move, ['BACK'], 1],
                    [self.monster.move, ['LEFT'], 1],
                    [self.monster.grab, ["all"], 3],
                    [self.monster.light, ["1"], 3]
                ],
                weights=[1, 1, 1, 1, 3, 3])

    def test_drop_inventory_on_death(self):
        # arrange
        items = [Vial(), Candle()]
        self.monster.inventory = items
        # act
        pub.sendMessage("died", character=self.monster)
        # assert
        self.assertEqual(items, self.monster.current_room.items)

    def test_heard(self):
        for key in RelativeDirection:
            with self.subTest():
                # arrange
                self.monster.heard_player = list()
                # act
                pub.sendMessage("Character.distant_description", direction=key)
                # assert
                self.assertEqual(key, self.monster.heard_player[0])

    def test_heard_nothing(self):
        # arrange
        self.monster.heard_player = list()
        # act
        pub.sendMessage("Character.distant_description", direction=None)
        # assert
        self.assertEqual(0, len(self.monster.heard_player))


if __name__ == '__main__':
    unittest.main()
