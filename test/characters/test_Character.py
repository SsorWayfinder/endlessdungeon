import unittest
from unittest import TestCase
from unittest import mock
from pubsub import pub

from endlessdungeon.CardinalDirection import CardinalDirection
from endlessdungeon.characters.Character import Character
from endlessdungeon.characters.Monster import Monster
from endlessdungeon.Command import Command
from endlessdungeon.Room import Room
from endlessdungeon.items.Vial import Vial
from endlessdungeon.items.Candle import Candle


class TestCharacter(TestCase):

    def setUp(self):
        self.character = Character()

    def test_init(self):
        # assert
        self.assertEqual(self.character.thirst, 0)
        self.assertTrue(self.character.is_alive)
        assert isinstance(self.character.current_room, Room)
        self.assertEqual(CardinalDirection.PY, self.character.orientation)
        self.assertEqual(0, self.character.current_room.x)
        self.assertEqual(0, self.character.current_room.y)
        assert isinstance(self.character.inventory, list)
        characters = self.character.current_room.get_characters()
        self.assertTrue(characters.__contains__(self.character))
        self.assertIsInstance(self.character.inventory[0], Candle)
        self.assertTrue(self.character.inventory[0].is_lit)

    def test_drink_no_thirst(self):
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.drink')

        vial = Vial()
        self.character.current_room.items.append(vial)
        # act
        result = self.character.drink(vial)
        # assert
        self.assertEqual(self.character.thirst, 0)
        self.assertTrue(vial.is_full)
        self.assertTrue(has_been_called[0])
        self.assertEqual("not_thirsty", states[0])

    def test_drink_no_vial(self):
        # arrange
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.drink')

        self.character.thirst = 1
        self.character.current_room.items = []
        # act
        result = self.character.drink()
        # assert
        self.assertEqual(1, self.character.thirst)
        self.assertTrue(has_been_called[0])
        self.assertEqual("nothing_to_drink", states[0])

    thirst_levels_drink = [1, 2, 3, 4]

    def test_drink_with_thirst(self):
        for level in self.thirst_levels_drink:
            with self.subTest():
                # arrange
                has_been_called = [False]
                states = list()

                def listener(state):
                    has_been_called[0] = True
                    states.append(state)

                pub.subscribe(listener, 'Character.drink')

                self.character.thirst = level
                vial = Vial()
                self.character.current_room.items = [vial]
                # act
                result = self.character.drink()
                # assert
                self.assertEqual(self.character.thirst, level - 1)
                self.assertTrue(has_been_called[0])
                self.assertEqual("success", states[0])

    def test_drink_dead(self):
        # arrange
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.drink')

        self.character.thirst = 1
        self.character.is_alive = False
        vail = Vial()
        self.character.current_room.items.append(vail)
        # act
        self.character.drink()
        # assert
        self.assertEqual(self.character.thirst, 1)
        self.assertTrue(vail.is_full)
        self.assertEqual(False, has_been_called[0])
        self.assertEqual(0, len(states))

    def test_drink_from_inventory(self):
        # arrange
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.drink')

        self.character.thirst = 1
        self.character.current_room.items = list()
        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        # act
        self.character.drink()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("success", states[0])
        self.assertEqual(0, self.character.thirst)
        self.assertFalse(self.character.inventory[1].is_full)

    def test_drink_from_inventory_with_empty_vial_in_room(self):
        # arrange
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.drink')

        self.character.thirst = 1
        self.character.current_room.items = list()
        empty_vial = Vial()
        empty_vial.is_full = False
        self.character.current_room.items.append(empty_vial)
        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        # act
        result = self.character.drink()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("success", states[0])
        self.assertEqual(0, self.character.thirst)
        self.assertFalse(self.character.inventory[1].is_full)

    def test_drink_from_inventory_with_empty_vial_in_inventory(self):
        # arrange
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.drink')

        self.character.thirst = 1
        self.character.current_room.items = list()
        empty_vial = Vial()
        empty_vial.is_full = False
        self.character.inventory.append(empty_vial)
        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        # act
        result = self.character.drink()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("success", states[0])
        self.assertEqual(0, self.character.thirst)
        self.assertFalse(self.character.inventory[1].is_full)

    def test_drink_from_inventory_with_empty_vial_in_room_and_inventory(self):
        # arrange
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.drink')

        self.character.thirst = 1
        self.character.current_room.items = list()
        empty_vial1 = Vial()
        empty_vial1.is_full = False
        self.character.current_room.items.append(empty_vial1)
        empty_vial2 = Vial()
        empty_vial2.is_full = False
        self.character.inventory.append(empty_vial2)
        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        # act
        result = self.character.drink()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("success", states[0])
        self.assertEqual(0, self.character.thirst)
        self.assertFalse(self.character.inventory[1].is_full)

    # TODO drink from not vial

    thirst_levels_exert = [0, 1, 2, 3]

    def test_exert(self):
        for level in self.thirst_levels_exert:
            with self.subTest():
                # arrange
                self.character.thirst = level
                # act
                self.character.exert()
                # assert
                self.assertEqual(self.character.thirst, level + 1)
                self.assertTrue(self.character.is_alive)

    def test_exert_to_death(self):
        # arrange
        self.character.thirst = 4
        # act
        self.character.exert()
        # assert
        self.assertEqual(self.character.is_alive, False)

    def test_exert_after_death(self):
        # arrange
        self.character.thirst = 4
        self.character.is_alive = False
        # act
        self.character.exert()
        # assert
        self.assertEqual(self.character.thirst, 4)

    number_of_exits = [1, 2, 3, 4]

    start_orientation_direction_description_end_orientation = [
        [CardinalDirection.PY, "forward", CardinalDirection.PY],
        [CardinalDirection.PX, "forward", CardinalDirection.PX],
        [CardinalDirection.NY, "forward", CardinalDirection.NY],
        [CardinalDirection.NX, "forward", CardinalDirection.NX],

        [CardinalDirection.PY, "right", CardinalDirection.PX],
        [CardinalDirection.PX, "right", CardinalDirection.NY],
        [CardinalDirection.NY, "right", CardinalDirection.NX],
        [CardinalDirection.NX, "right", CardinalDirection.PY],

        [CardinalDirection.NX, "back", CardinalDirection.PX],
        [CardinalDirection.NY, "back", CardinalDirection.PY],
        [CardinalDirection.PX, "back", CardinalDirection.NX],
        [CardinalDirection.PY, "back", CardinalDirection.NY],

        [CardinalDirection.PY, "left", CardinalDirection.NX],
        [CardinalDirection.PX, "left", CardinalDirection.PY],
        [CardinalDirection.NY, "left", CardinalDirection.PX],
        [CardinalDirection.NX, "left", CardinalDirection.NY],
    ]

    def test_command_move(self):
        # arrange
        for data in self.start_orientation_direction_description_end_orientation:
            with self.subTest():
                self.setUp()
                has_been_called = [False]
                states = list()
                directions = list()

                def listener(state, direction=None):
                    has_been_called[0] = True
                    states.append(state)
                    directions.append(direction)

                pub.subscribe(listener, 'Character.move')

                has_been_called2 = [False]
                rooms = list()

                def listener2(room):
                    has_been_called2[0] = True
                    rooms.append(room)

                pub.subscribe(listener2, 'Character.Room.description')

                expected_room = mock.MagicMock()

                def side_effect(value):
                    if value == data[2]:
                        return expected_room

                mock_get_exit = mock.MagicMock(side_effect=side_effect)
                self.character.current_room = mock.MagicMock()
                self.character.current_room.is_difficult = False
                self.character.current_room.get_exit = mock_get_exit
                mock_remove = mock.MagicMock()
                self.character.current_room.remove_character = mock_remove
                self.character.cardinal_mapping.rotate(4 - self.character.cardinal_mapping.index(data[0]))
                # act
                self.character.command(Command.MOVE, [data[1]])
                #
                mock_get_exit.assert_called_with(data[2])
                mock_remove.assert_called_with(self.character)
                self.assertTrue(has_been_called[0])
                self.assertEqual("travel", states[0])
                self.assertEqual(data[1].upper(), directions[0])
                self.assertEqual(self.character.current_room, expected_room)
                self.assertEqual(data[2], self.character.orientation)
                self.assertTrue(has_been_called2[0])
                self.assertEqual(expected_room, rooms[0])

    def test_command_move_up(self):
        # arrange
        has_been_called = [False]
        characters = list()

        def listener(character):
            has_been_called[0] = True
            characters.append(character)

        pub.subscribe(listener, "escape")
        # act
        self.character.command(Command.MOVE, ["UP"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual(self.character, characters[0])

    def test_command_move_bad(self):
        # arrange
        has_been_called = [False]
        states = list()
        directions = list()

        def listener(state, direction=None):
            has_been_called[0] = True
            states.append(state)
            directions.append(direction)

        pub.subscribe(listener, 'Character.move')

        expected_room = Room(0, -1)
        self.character.current_room.exits = list()
        self.character.current_room.exits.append(expected_room)
        # act
        self.character.command(Command.MOVE, ["", "bad"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("bad_input", states[0])
        self.assertEqual("FORWARD\nRIGHT\nBACK\nLEFT\nUP\n", directions[0])

    def test_command_move_difficult(self):
        # arrange
        has_been_called = [False]
        states = list()
        directions = list()

        def listener(state, direction=None):
            has_been_called[0] = True
            states.append(state)
            directions.append(direction)

        pub.subscribe(listener, 'Character.move')

        has_been_called2 = [False]
        rooms = list()

        def listener2(room):
            has_been_called2[0] = True
            rooms.append(room)

        pub.subscribe(listener2, 'Character.Room.description')

        next_room = Room(-1, 0)
        self.character.current_room.is_difficult = True
        self.character.current_room.exits = [None, next_room]
        # act
        self.character.move(["left"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("difficult", states[0])
        self.assertEqual("travel", states[1])
        self.assertEqual("LEFT", directions[1])
        self.assertEqual(1, self.character.thirst)
        self.assertTrue(has_been_called2[0])
        self.assertEqual(next_room, rooms[0])

    def test_command_move_difficult_move_back(self):
        # arrange
        has_been_called = [False]
        states = list()
        directions = list()

        def listener(state, direction=None):
            has_been_called[0] = True
            states.append(state)
            directions.append(direction)

        pub.subscribe(listener, 'Character.move')

        has_been_called2 = [False]
        rooms = list()

        def listener2(room):
            has_been_called2[0] = True
            rooms.append(room)

        pub.subscribe(listener2, 'Character.Room.description')

        next_room = Room(0, -1)
        self.character.current_room.is_difficult = True
        self.character.current_room.exits = [None, next_room]
        # act
        self.character.move(["back"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("travel", states[0])
        self.assertEqual("BACK", directions[0])
        self.assertEqual(0, self.character.thirst)
        self.assertTrue(has_been_called2[0])
        self.assertEqual(next_room, rooms[0])

    def test_command_move_difficult_killing(self):
        # arrange
        has_been_called = [False]
        states = list()
        directions = list()

        def listener(state, direction=None):
            has_been_called[0] = True
            states.append(state)
            directions.append(direction)

        pub.subscribe(listener, 'Character.move')

        has_been_called2 = [False]
        rooms = list()

        def listener2(room):
            has_been_called2[0] = True
            rooms.append(room)

        pub.subscribe(listener2, 'Character.Room.description')

        next_room = Room(-1, 0)
        self.character.current_room.is_difficult = True
        self.character.current_room.exits = [None, next_room]
        self.character.thirst = 4
        # act
        self.character.move(["left"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("difficult", states[0])
        self.assertEqual("exhausted", states[1])
        self.assertEqual(5, self.character.thirst)
        self.assertFalse(self.character.is_alive)
        self.assertFalse(has_been_called2[0])

    def test_command_look_bad_input(self):
        # arrange
        has_been_called = [False]
        states = list()
        directions = list()

        def listener(state, direction=None):
            has_been_called[0] = True
            states.append(state)
            directions.append(direction)

        pub.subscribe(listener, 'Character.look')
        # act
        self.character.look(["garbage"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("bad_input", states[0])
        self.assertEqual("FORWARD\nRIGHT\nBACK\nLEFT\n", directions[0])

    def test_command_look_no_exit(self):
        # arrange
        has_been_called = [False]
        states = list()
        directions = list()

        def listener(state, direction=None):
            has_been_called[0] = True
            states.append(state)
            directions.append(direction)

        pub.subscribe(listener, 'Character.look')

        self.character.current_room.exits = list()
        # act
        self.character.look(["left"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("no_exit", states[0])

    def test_command_look_success(self):
        # arrange
        has_been_called = [False]
        states = list()
        directions = list()

        def listener(state, direction=None):
            has_been_called[0] = True
            states.append(state)
            directions.append(direction)

        pub.subscribe(listener, 'Character.look')

        has_been_called2 = [False]
        rooms = list()

        def listener2(room):
            has_been_called2[0] = True
            rooms.append(room)

        pub.subscribe(listener2, 'Character.Room.description')

        self.character.current_room.exits = list()
        next_room = Room(-1, 0)
        self.character.current_room.exits = [None, next_room]
        # act
        self.character.look(["left"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("success", states[0])
        self.assertTrue(has_been_called[0])
        self.assertEqual(next_room, rooms[0])
        self.assertTrue(next_room.has_been_generated)

    def test_command_grab(self):
        # arrange
        has_been_called = [False]
        states = list()
        items = list()

        def listener(state, item=None):
            has_been_called[0] = True
            states.append(state)
            items.append(item)

        pub.subscribe(listener, 'Character.grab')

        expected_vial = Vial()
        self.character.current_room.items = list()
        self.character.current_room.items.append(expected_vial)
        # act
        self.character.command(Command.GRAB, ["grab"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("grabbed", states[0])
        self.assertEqual(expected_vial, items[0][0])
        self.assertEqual(expected_vial, self.character.inventory[1])

    def test_command_inventory(self):
        # arrange
        has_been_called = [False]
        items_list = list()

        def listener(items):
            has_been_called[0] = True
            items_list.append(items)

        pub.subscribe(listener, 'Character.inventory')
        # act
        result = self.character.command(Command.INVENTORY, [])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertIsInstance(items_list[0][0], Candle)

    def test_command_inventory_with_vial(self):
        # arrange
        has_been_called = [False]
        items_list = list()

        def listener(items):
            has_been_called[0] = True
            items_list.append(items)

        pub.subscribe(listener, 'Character.inventory')

        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        # act
        self.character.command(Command.INVENTORY, [])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertIsInstance(items_list[0][0], Candle)
        self.assertEqual(expected_vial, items_list[0][1])

    def test_command_drop(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        self.character.inventory.remove(self.character.inventory[0])
        # act
        self.character.command(Command.DROP, [])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("dropped", states[0])
        self.assertEqual(expected_vial, items_list[0][0])

    def test_command_drop_position(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        self.character.inventory.append(Vial())
        # act
        self.character.command(Command.DROP, ["2"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("dropped", states[0])
        self.assertEqual(expected_vial, items_list[0][0])

    def test_drop_empty_inventory(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        self.character.inventory = list()
        # act
        self.character.drop([])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("empty", states[0])

    def test_drop_no_position_given(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        has_been_called2 = [False]
        items_list2 = list()

        def listener2(items):
            has_been_called2[0] = True
            items_list2.append(items)

        pub.subscribe(listener2, 'Character.inventory')

        self.character.inventory.append(Vial())
        self.character.inventory.append(Vial())
        # act
        self.character.drop([])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("undefined", states[0])
        self.assertTrue(has_been_called2[0])
        self.assertEqual(self.character.inventory, items_list2[0])

    def test_drop_only_one_thing(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        expected_vial = Vial()
        self.character.inventory = list()
        self.character.inventory.append(expected_vial)
        self.character.current_room.items = list()
        # act
        self.character.drop([])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("dropped", states[0])
        self.assertEqual(expected_vial, items_list[0][0])
        self.assertEqual(expected_vial, self.character.current_room.items[0])

    def test_drop_second_thing(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        expected_vial = Vial()
        expected_vial.is_full = False
        self.character.inventory.append(expected_vial)

        self.character.current_room.items = list()
        # act
        self.character.drop(["2"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("dropped", states[0])
        self.assertEqual(expected_vial, items_list[0][0])
        self.assertEqual(expected_vial, self.character.current_room.items[0])

    def test_drop_many(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        expected_vial = Vial()
        expected_vial2 = Vial()
        expected_vial2.is_full = False
        self.character.inventory.append(Vial())
        self.character.inventory.append(expected_vial)
        self.character.inventory.append(expected_vial2)
        self.character.current_room.items = list()
        # act
        self.character.drop(["3", "4"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("dropped", states[0])
        self.assertTrue(self.character.current_room.items.__contains__(expected_vial))
        self.assertTrue(self.character.current_room.items.__contains__(expected_vial2))
        self.assertTrue(items_list[0].__contains__(expected_vial2))
        self.assertTrue(items_list[0].__contains__(expected_vial))

    def test_drop_all(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        expected_vial1 = Vial()
        expected_vial2 = Vial()
        expected_vial3 = Vial()
        expected_vial3.is_full = False
        self.character.inventory.append(expected_vial1)
        self.character.inventory.append(expected_vial2)
        self.character.inventory.append(expected_vial3)
        self.character.current_room.items = list()
        # act
        self.character.drop(["all"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("dropped", states[0])
        self.assertEqual(4, len(items_list[0]))
        self.assertTrue(self.character.current_room.items.__contains__(expected_vial1))
        self.assertTrue(self.character.current_room.items.__contains__(expected_vial2))
        self.assertTrue(self.character.current_room.items.__contains__(expected_vial3))

    def test_drop_position_not_had(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, items=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(items)

        pub.subscribe(listener, 'Character.drop')

        self.character.inventory.append(Vial())
        self.character.inventory.append(Vial())

        self.character.current_room.items = list()
        # act
        self.character.drop(["5"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("no_item", states[0])
        self.assertEqual(3, len(self.character.inventory))
        self.assertEqual(0, len(self.character.current_room.items))

    def test_description_thirst(self):
        # arrange
        for index in range(4):
            with self.subTest():
                self.setUp()
                has_been_called = [False]
                states = list()

                def listener(state):
                    has_been_called[0] = True
                    states.append(state)

                pub.subscribe(listener, 'Character.description.thirst')

                self.character.thirst = index
                # act
                self.character.description()
                # assert
                self.assertTrue(has_been_called[0])
                self.assertEqual(index, states[0])

    def test_description_unlit(self):
        # arrange
        has_been_called = [False]

        def listener(directions=None):
            has_been_called[0] = True

        pub.subscribe(listener, 'Character.description.exits')

        self.character.inventory = list()
        self.character.current_room = mock.MagicMock()
        self.character.current_room.is_lit = mock.MagicMock(return_value=False)
        # act
        self.character.description()
        # assert
        self.assertEqual(False, has_been_called[0])

    exits_words = \
        [
            [0, []],
            [1, [0], [-1], [2]],
            [2, [0, -1], [-1, 0], [3, 2]],
            [3, [0, -1, 0], [-1, 0, 1], [3, 0, 2]],
            [4, [0, -1, 0, 1], [-1, 0, 1, 0], [3, 0, 1, 2]],
            [1, [-1], [0], [3]],
            [1, [0], [1], [0]],
            [1, [1], [0], [1]],
        ]

    def test_description_exits(self):
        for data in self.exits_words:
            with self.subTest():
                # arrange
                has_been_called = [False]
                directions_list = list()

                def listener(directions):
                    has_been_called[0] = True
                    directions_list.append(directions)

                pub.subscribe(listener, 'Character.description.exits')

                Room.room_grid = dict()
                self.setUp()
                self.character.current_room.exits = list()
                for i in range(data[0]):
                    self.character.current_room.exits.append(Room(data[1][i], data[2][i]))
                # act
                result = self.character.description()
                # assert
                self.assertTrue(has_been_called[0])
                self.assertEqual(data[-1], directions_list[0])

    exits_with_monster = \
        [
            [0, -1, 2],
            [-1, 0, 3],
            [0, 1, 0],
            [1, 0, 1],

        ]

    def test_description_exits_with_monster(self):
        for data in self.exits_with_monster:
            with self.subTest():
                # arrange
                has_been_called = [False]
                directions_list = list()

                def listener(direction):
                    has_been_called[0] = True
                    directions_list.append(direction)

                pub.subscribe(listener, 'Monster.description')

                Room.room_grid = dict()
                self.setUp()
                self.character.current_room.exits = list()
                monster_exit = Room(data[0], data[1])
                mock_monster = mock.MagicMock()
                type(mock_monster).__name__ = "Monster"
                monster_exit.add_character(mock_monster)
                self.character.current_room.exits.append(monster_exit)
                # act
                self.character.description()
                # assert
                self.assertTrue(has_been_called[0])
                self.assertEqual(data[-1], directions_list[0])

    def test_description_exits_with_monster_next_to_it(self):
        for data in self.exits_with_monster:
            with self.subTest():
                # arrange
                has_been_called = [False]
                directions_list = list()

                def listener(direction):
                    has_been_called[0] = True
                    directions_list.append(direction)

                pub.subscribe(listener, 'Monster.distant_description')

                Room.room_grid = dict()
                self.setUp()
                self.character.current_room.exits = list()
                near_exit = Room(data[0], data[1])
                mock_monster = mock.MagicMock()
                type(mock_monster).__name__ = "Monster"
                monster_exit = Room(data[0]+1, data[1])
                monster_exit.add_character(mock_monster)
                near_exit.exits.append(monster_exit)
                self.character.current_room.exits.append(near_exit)
                # act
                self.character.description()
                # assert
                self.assertTrue(has_been_called[0])
                self.assertEqual(data[-1], directions_list[0])

    def test_description_exit_with_character_next_to_it(self):
        # arrange
        has_been_called = [False]
        directions_list = list()

        def listener(direction):
            has_been_called[0] = True
            directions_list.append(direction)

        pub.subscribe(listener, 'Character.distant_description')

        Room.room_grid = dict()
        self.character.current_room.exits = list()
        near_exit = Room(0, -1)
        mock_character = mock.MagicMock()
        type(mock_character).__name__ = "Character"
        monster_exit = Room(1, -1)
        monster_exit.add_character(mock_character)
        near_exit.exits.append(monster_exit)
        self.character.current_room.exits.append(near_exit)
        # act
        self.character.description()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual(2, directions_list[0])

    def test_singular_exit_with_nones(self):
        # arrange
        has_been_called = [False]
        directions_list = list()

        def listener(directions):
            has_been_called[0] = True
            directions_list.append(directions)

        pub.subscribe(listener, 'Character.description.exits')

        self.character.current_room.exits = list()
        self.character.current_room.exits.append(None)
        self.character.current_room.exits.append(Room(1, 0))
        # act
        result = self.character.description()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual([1], directions_list[0])

    def test_move_bad_direction(self):
        # arrange
        has_been_called = [False]
        states = list()
        directions_list = list()

        def listener(state, direction=None):
            has_been_called[0] = True
            states.append(state)
            directions_list.append(direction)

        pub.subscribe(listener, 'Character.move')

        self.character.current_room.exits = list()
        self.character.current_room.exits.append(None)
        self.character.current_room.exits.append(Room(1, 0))
        # act
        self.character.move(["left"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("no_exit", states[0])

    def test_grab_bad(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, item=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(item)

        pub.subscribe(listener, 'Character.grab')

        self.character.current_room.items = list()
        self.character.inventory = list()
        # act
        self.character.grab()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("empty", states[0])
        self.assertEqual(0, len(self.character.inventory))

    def test_grab_full_vial(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, item=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(item)

        pub.subscribe(listener, 'Character.grab')

        expected_vial = Vial()
        self.character.current_room.items = list()
        self.character.current_room.items.append(expected_vial)
        # act
        result = self.character.grab()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("grabbed", states[0])
        self.assertEqual(expected_vial, items_list[0][0])
        self.assertEqual(expected_vial, self.character.inventory[1])

    def test_grab_no_position(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, item=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(item)

        pub.subscribe(listener, 'Character.grab')

        self.character.current_room.items = list()
        self.character.current_room.items.append(Vial())
        self.character.current_room.items.append(Vial())
        # act
        result = self.character.grab()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("undefined", states[0])

    def test_grab_one_of_many_vials(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, item=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(item)

        pub.subscribe(listener, 'Character.grab')

        unexpected_vial = Vial()
        unexpected_vial.is_full = False
        self.character.current_room.items = list()
        self.character.current_room.items.append(unexpected_vial)
        expected_vial = Vial()
        self.character.current_room.items.append(expected_vial)
        # act
        result = self.character.grab(["2"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("grabbed", states[0])
        self.assertEqual(expected_vial, items_list[0][0])
        self.assertEqual(expected_vial, self.character.inventory[1])

    def test_grab_many_vials(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, item=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(item)

        pub.subscribe(listener, 'Character.grab')

        self.character.current_room.items = list()
        unexpected_vial = Vial()
        unexpected_vial.is_full = False
        self.character.current_room.items.append(Vial())
        self.character.current_room.items.append(unexpected_vial)
        self.character.current_room.items.append(Vial())
        # act
        result = self.character.grab(["1", "3"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("grabbed", states[0])
        self.assertFalse(self.character.inventory.__contains__(unexpected_vial))
        self.assertEqual(2, len(items_list[0]))

    def test_grab_all_vials(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, item=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(item)

        pub.subscribe(listener, 'Character.grab')

        self.character.current_room.items = list()
        expected_vial = Vial()
        expected_vial.is_full = False
        self.character.current_room.items.append(Vial())
        self.character.current_room.items.append(expected_vial)
        self.character.current_room.items.append(Vial())
        # act
        result = self.character.grab(["all"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("grabbed", states[0])
        self.assertEqual(3, len(items_list[0]))

    def test_inventory(self):
        # arrange
        has_been_called = [False]
        items_list = list()

        def listener(items):
            has_been_called[0] = True
            items_list.append(items)

        pub.subscribe(listener, 'Character.inventory')

        self.character.inventory = list()
        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        # act
        result = self.character.inventory_check()
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual(expected_vial, items_list[0][0])

    def test_grab_invalid_position(self):
        # arrange
        has_been_called = [False]
        states = list()
        items_list = list()

        def listener(state, item=None):
            has_been_called[0] = True
            states.append(state)
            items_list.append(item)

        pub.subscribe(listener, 'Character.grab')

        expected_vial = Vial()
        self.character.current_room.items = list()
        self.character.current_room.items.append(expected_vial)
        # act
        self.character.grab(["1", "2"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("no_item", states[0])

    def test_event_moved(self):
        # arrange
        has_been_called = [False]
        called_character = list()

        def listener(character):
            called_character.append(character)
            has_been_called[0] = True

        pub.subscribe(listener, 'moved')
        next_room = Room(-1, 0)
        self.character.current_room.exits = [None, next_room]
        # act
        self.character.move(["left"])
        # assert
        self.assertEqual(called_character[0], self.character)
        self.assertTrue(has_been_called[0])

    def test_event_drank(self):
        # arrange
        has_been_called = [False]
        called_character = list()

        def listener(character):
            called_character.append(character)
            has_been_called[0] = True

        pub.subscribe(listener, 'drank')
        self.character.inventory.append(Vial())
        self.character.thirst = 1
        # act
        self.character.drink()
        # assert
        self.assertEqual(called_character[0], self.character)
        self.assertTrue(has_been_called[0])

    def test_event_exerted(self):
        # arrange
        has_been_called = [False]
        called_character = list()

        def listener(character):
            called_character.append(character)
            has_been_called[0] = True

        pub.subscribe(listener, 'exerted')
        # act
        self.character.exert()
        # assert
        self.assertEqual(called_character[0], self.character)
        self.assertTrue(has_been_called[0])

    def test_event_died(self):
        # arrange
        has_been_called = [False]
        called_character = list()

        def listener(character):
            called_character.append(character)
            has_been_called[0] = True

        pub.subscribe(listener, 'died')
        # act
        self.character.is_alive = False
        # assert
        self.assertEqual(called_character[0], self.character)
        self.assertTrue(has_been_called[0])

    def test_light(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        self.character.inventory.append(Vial())
        self.character.inventory.append(Candle())
        # act
        result = self.character.light([])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("success", states[0])
        self.assertTrue(self.character.inventory[1].is_lit)

    def test_light_position(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        self.character.inventory.append(Candle())
        # act
        result = self.character.light(["1"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertTrue(self.character.inventory[0].is_lit)
        self.assertEqual("success_position", states[0])
        self.assertEqual("1", positions[0])

    def test_light_position_needed(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        self.character.inventory.append(Vial())
        self.character.inventory.append(Candle())
        self.character.inventory.append(Vial())
        self.character.inventory.append(Candle())
        self.character.inventory.append(Vial())
        # act
        result = self.character.light([])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("undefined", states[0])

    def test_light_second(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        self.character.inventory.append(Candle())
        self.character.inventory.append(Candle())
        # act
        result = self.character.light(["2"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertTrue(self.character.inventory[1].is_lit)
        self.assertEqual(False, self.character.inventory[0].is_lit)
        self.assertEqual("success_position", states[0])
        self.assertEqual("2", positions[0])

    def test_light_all(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        self.character.inventory.append(Candle())
        self.character.inventory.append(Candle())
        # act
        result = self.character.light(["1", "2"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertTrue(self.character.inventory[1].is_lit)
        self.assertTrue(self.character.inventory[0].is_lit)
        self.assertEqual("success_position", states[0])
        self.assertEqual("1", positions[0])
        self.assertEqual("success_position", states[1])
        self.assertEqual("2", positions[1])

    def test_light_non_candle(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        expected_vial = Vial()
        self.character.inventory.append(expected_vial)
        # act
        self.character.light([])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("no_candle", states[0])

    def test_light_non_candle_position(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        self.character.inventory.append(Vial())
        self.character.inventory.append(Vial())
        # act
        self.character.light(["1", "2"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("not_candle", states[0])
        self.assertEqual("1", positions[0])
        self.assertIsInstance(items_list[0], Vial)
        self.assertEqual("not_candle", states[1])
        self.assertEqual("2", positions[1])
        self.assertIsInstance(items_list[1], Vial)

    def test_light_non_number(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        self.character.inventory.append(Vial())
        # act
        result = self.character.light(["a", "b"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("bad_input", states[0])
        self.assertEqual("a", positions[0])
        self.assertEqual("bad_input", states[1])
        self.assertEqual("b", positions[1])

    def test_light_bad_position(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.light')

        self.character.inventory = list()
        self.character.inventory.append(Vial())
        # act
        result = self.character.light(["2", "9"])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("no_item", states[0])
        self.assertEqual("2", positions[0])
        self.assertEqual("no_item", states[1])
        self.assertEqual("9", positions[1])

    def test_extinguish(self):
        # arrange
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.extinguish')

        self.character.inventory = list()
        self.character.inventory.append(Candle())
        self.character.inventory[0].is_lit = True
        # act
        self.character.extinguish([])
        # assert
        self.assertEqual(False, self.character.inventory[0].is_lit)
        self.assertTrue(has_been_called[0])
        self.assertEqual("success", states[0])

    def test_extinguish_mixed(self):
        # arrange
        has_been_called = [False]
        states = list()

        def listener(state):
            has_been_called[0] = True
            states.append(state)

        pub.subscribe(listener, 'Character.extinguish')

        self.character.inventory = list()
        self.character.inventory.append(Candle())
        self.character.inventory[0].is_lit = True
        self.character.inventory.append(Candle())
        # act
        result = self.character.extinguish([])
        # assert
        self.assertEqual(False, self.character.inventory[0].is_lit)
        self.assertTrue(has_been_called[0])
        self.assertEqual("success", states[0])

    def test_extinguish_none(self):
        # arrange
        has_been_called = [False]
        states = list()
        positions = list()
        items_list = list()

        def listener(state, position=None, item=None):
            has_been_called[0] = True
            states.append(state)
            positions.append(position)
            items_list.append(item)

        pub.subscribe(listener, 'Character.extinguish')

        self.character.inventory = list()
        # act
        result = self.character.extinguish([])
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual("failure", states[0])

    def test_say(self):
        # arrange
        has_been_called = [False]
        word_list = list()
        characters = list()

        def listener(said, character):
            has_been_called[0] = True
            word_list.append(said)
            characters.append(character)

        pub.subscribe(listener, 'say')

        words = ["hello?", "anyone", "there?"]
        # act
        self.character.say(words)
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual(words, word_list[0])
        self.assertEqual(self.character, characters[0])

    def test_refill(self):
        # arrange
        has_been_called = [False]
        characters = list()

        def listener(character):
            has_been_called[0] = True
            characters.append(character)
        pub.subscribe(listener, "refill")
        # act
        self.character.refill(None)
        # assert
        self.assertTrue(has_been_called[0])
        self.assertEqual(characters[0], self.character)


if __name__ == '__main__':
    unittest.main()
